use std::{collections::HashMap, sync::Arc};

use specs::prelude::*;

use ggez::{
    graphics::{DrawParam, FilterMode, Image, Rect},
    Context,
};

pub enum TextureSize {
    Full,
    Size(u32, u32),
}

pub struct Texture {
    image: Arc<Image>,
    texture_size: (u32, u32),
    image_size: (u32, u32),
}

impl Texture {
    pub fn new(image: Image, texture_size: TextureSize) -> Self {
        let image_size = (image.width(), image.height());

        let texture_size = match texture_size {
            TextureSize::Full => image_size,
            TextureSize::Size(x, y) => (x, y),
        };

        Self {
            image: Arc::new(image),
            texture_size,
            image_size,
        }
    }

    pub fn image(&self) -> &Arc<Image> {
        &self.image
    }

    pub fn texture_size(&self) -> (u32, u32) {
        self.texture_size
    }

    pub fn image_size(&self) -> (u32, u32) {
        self.image_size
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum AssetKey {
    BabyDrunk,
    Chicken,
    Grass,
    ChickenAnimated,
    Cobblestone,
    RedSyringe,
    Trampoline,
    Heart,
    BrokenBottle,
    TrapClosed,
    TrapOpened,
    Ladder,
    Spikes,
}

pub trait Selectable<T> {
    fn asset_key_for(&self, key: &T) -> &AssetKey;
}

pub struct Assets {
    assets: HashMap<AssetKey, Texture>,
}

impl Assets {
    pub fn new(ctx: &mut Context) -> Self {
        let mut assets: HashMap<AssetKey, Texture> = HashMap::new();

        assets.insert(
            AssetKey::BabyDrunk,
            Self::load_texture(ctx, "/baby_drunk.png", TextureSize::Full),
        );

        assets.insert(
            AssetKey::Chicken,
            Self::load_texture(ctx, "/chicken.png", TextureSize::Size(32, 32)),
        );

        assets.insert(
            AssetKey::Grass,
            Self::load_texture(ctx, "/grass.png", TextureSize::Full),
        );

        assets.insert(
            AssetKey::ChickenAnimated,
            Self::load_texture(ctx, "/chicken_animated.png", TextureSize::Size(32, 32)),
        );

        assets.insert(
            AssetKey::Cobblestone,
            Self::load_texture(ctx, "/cobblestone.png", TextureSize::Full),
        );

        assets.insert(
            AssetKey::RedSyringe,
            Self::load_texture(ctx, "/red_syringe.png", TextureSize::Full),
        );

        assets.insert(
            AssetKey::Trampoline,
            Self::load_texture(ctx, "/trampoline.png", TextureSize::Size(32, 32)),
        );

        assets.insert(
            AssetKey::Heart,
            Self::load_texture(ctx, "/heart.png", TextureSize::Size(16, 16)),
        );

        assets.insert(
            AssetKey::BrokenBottle,
            Self::load_texture(ctx, "/broken_bottle.png", TextureSize::Full),
        );

        assets.insert(
            AssetKey::TrapClosed,
            Self::load_texture(ctx, "/trap_closed.png", TextureSize::Full),
        );

        assets.insert(
            AssetKey::TrapOpened,
            Self::load_texture(ctx, "/trap_opened.png", TextureSize::Full),
        );

        assets.insert(
            AssetKey::Ladder,
            Self::load_texture(ctx, "/ladder.png", TextureSize::Full),
        );

        assets.insert(
            AssetKey::Spikes,
            Self::load_texture(ctx, "/spikes.png", TextureSize::Full),
        );

        Self { assets }
    }

    fn load_texture(ctx: &mut Context, path: &str, texture_size: TextureSize) -> Texture {
        let mut image = Image::new(ctx, path).expect(format!("Could not load {}", path).as_str());
        image.set_filter(FilterMode::Nearest);

        Texture::new(image, texture_size)
    }

    pub fn texture_for(&self, asset_key: &AssetKey) -> &Texture {
        self.assets.get(asset_key).expect(
            format!(
                "Couldn't get texture for {:?} from assets resources",
                asset_key
            )
            .as_str(),
        )
    }

    pub fn get_draw_params(&self, image_key: &AssetKey, tile_number: Option<u32>) -> DrawParam {
        let texture = self.texture_for(image_key);
        let image_size = texture.image_size();
        let texture_size = texture.texture_size();
        let tile_count = (image_size.0 / texture_size.0, image_size.1 / texture_size.1);

        match tile_number {
            None => DrawParam::default(),
            Some(i) => {
                let tile_pos_x = (i % tile_count.0) as f32;
                let tile_pos_y = (i / tile_count.0) as f32;

                if tile_pos_y >= tile_count.1 as f32 {
                    panic!("Tile {} does not exists on {:?}", i, image_key);
                }

                let rect = Rect::new(
                    tile_pos_x / tile_count.0 as f32,
                    tile_pos_y / tile_count.1 as f32,
                    1. / tile_count.0 as f32,
                    1. / tile_count.1 as f32,
                );

                DrawParam {
                    src: rect,
                    ..Default::default()
                }
            }
        }
    }
}

impl Default for Assets {
    fn default() -> Self {
        let assets: HashMap<AssetKey, Texture> = HashMap::new();

        Self { assets }
    }
}
