use ggez::{
    event,
    event::{Axis, Button, Keycode, Mod},
    graphics, timer, Context, GameResult,
};

use crate::{consts::*, Assets};

pub struct MenuState {}

impl MenuState {
    pub fn new(ctx: &mut Context) -> GameResult<MenuState> {
        Ok(MenuState {})
    }

    pub fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }

    pub fn draw(&mut self, ctx: &mut Context, assets: &Assets) -> GameResult<()> {
        Ok(())
    }

    pub fn key_down_event(&mut self, ctx: &mut Context, keycode: Keycode, _keymod: Mod, repeat: bool) {
        if keycode == Keycode::Escape {
            ctx.quit().unwrap();
        }

        // systems::InputController::with_keycode(keycode, true).run_now(&self.world.res);
    }

    pub fn key_up_event(&mut self, _ctx: &mut Context, keycode: Keycode, _keymod: Mod, repeat: bool) {
        // systems::InputController::with_keycode(keycode, false).run_now(&self.world.res);
    }

    pub fn controller_button_down_event(&mut self, _ctx: &mut Context, btn: Button, instance_id: i32) {
        // systems::InputController::with_btn(instance_id, btn, true).run_now(&self.world.res);
    }

    pub fn controller_button_up_event(&mut self, _ctx: &mut Context, btn: Button, instance_id: i32) {
        // systems::InputController::with_btn(instance_id, btn, false).run_now(&self.world.res);
    }

    pub fn controller_axis_event(
        &mut self,
        _ctx: &mut Context,
        axis: Axis,
        value_norm: f32,
        instance_id: i32,
    ) {
        // systems::InputController::with_axis(instance_id, axis, value_norm).run_now(&self.world.res);
    }
}
