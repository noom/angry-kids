pub const FPS: u32 = 60;
pub const DT: f32 = 1. / FPS as f32;

// pub const SCREEN_WH: (u32, u32) = (1600, 900);
pub const SCREEN_WH: (u32, u32) = (800, 448);

// pub const SCALE_FACTOR: f32 = 50.;
pub const SCALE_FACTOR: f32 = 32.;

pub const MAP_WH: (usize, usize) = (
    SCREEN_WH.0 as usize / SCALE_FACTOR as usize,
    SCREEN_WH.1 as usize / SCALE_FACTOR as usize,
);

pub const NPHYSICS_WORLD_WH: (f32, f32) = (
    SCREEN_WH.0 as f32 / SCALE_FACTOR,
    SCREEN_WH.1 as f32 / SCALE_FACTOR,
);

pub const PEACE_WALL_DURATION: f32 = 10.;

pub const PLAYER_SPEED: f32 = 6.;
pub const CLIMBING_SPEED: f32 = 0.25;
pub const JUMP_HEIGHT: f32 = 2.3;
pub const JUMP_DURATION: f32 = 0.35;

pub const DAMAGED_DURATION: f32 = 1.;

pub const GRAVITY: f32 = -(2. * JUMP_HEIGHT / (JUMP_DURATION * JUMP_DURATION));

pub const COLLIDER_MARGIN: f32 = 0.01;

pub const TILE_SIZE: f32 = 1.;

pub const PLAYER_WH: (f32, f32) = (0.70, 0.90);

pub const CONSTRUCTION_TIME: f32 = 0.25;

pub const REMANENT_PROJECTILE_TIMER: f32 = 3.;

pub const LIFE_AMOUNT: f32 = 10.;

pub const JOYSTICK_THRESHOLD: f32 = 0.35;
pub const TRIGGER_SENSIBILITY: f32 = 0.6;

pub const SELECTOR_FADING_DURATION: f32 = 0.1;
pub const SELECTOR_DISPLAYING_DURATION: f32 = 1.;
pub const SELECTOR_TRANSPARENCY: f32 = 1.;
