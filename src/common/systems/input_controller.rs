use ggez::event::{Axis, Button, Keycode};

use specs::prelude::*;

use crate::common::{self as cmn, PlayerProfile};

pub struct InputController {
    instance_id: Option<i32>,
    btn: Option<Button>,
    axis: Option<(Axis, f32)>,
    keycode: Option<Keycode>,
    key_down: Option<bool>,
}

impl InputController {
    pub fn with_keycode(keycode: Keycode, key_down: bool) -> Self {
        Self {
            instance_id: None,
            btn: None,
            axis: None,
            keycode: Some(keycode),
            key_down: Some(key_down),
        }
    }

    pub fn with_btn(instance_id: i32, btn: Button, key_down: bool) -> Self {
        Self {
            instance_id: Some(instance_id),
            btn: Some(btn),
            axis: None,
            keycode: None,
            key_down: Some(key_down),
        }
    }

    pub fn with_axis(instance_id: i32, axis: Axis, value_norm: f32) -> Self {
        Self {
            instance_id: Some(instance_id),
            btn: None,
            axis: Some((axis, value_norm)),
            keycode: None,
            key_down: None,
        }
    }

    fn manage_keyboard_one(&self, input_control: &mut cmn::components::InputControl) {
        if let (Some(keycode), Some(key_down_bool)) = (self.keycode, self.key_down) {
            let key_down = key_down_bool as i32 as f32;

            match keycode {
                Keycode::W => input_control.set_moving_y(-1. * key_down),
                Keycode::S => input_control.set_moving_y(1. * key_down),
                Keycode::A => input_control.set_moving_x(-1. * key_down),
                Keycode::D => input_control.set_moving_x(1. * key_down),
                Keycode::Z => input_control.build = key_down_bool,
                Keycode::X => input_control.destroy = key_down_bool,
                Keycode::Q => input_control.prev_tile = key_down_bool,
                Keycode::E => input_control.next_tile = key_down_bool,
                Keycode::R => input_control.prev_projectile = key_down_bool,
                Keycode::T => input_control.next_projectile = key_down_bool,
                Keycode::Space => input_control.shoot = key_down_bool,
                _ => (),
            }
        }
    }

    fn manage_keyboard_two(&self, input_control: &mut cmn::components::InputControl) {
        if let (Some(keycode), Some(key_down_bool)) = (self.keycode, self.key_down) {
            let key_down = key_down_bool as i32 as f32;

            match keycode {
                Keycode::Up => input_control.set_moving_y(-1. * key_down),
                Keycode::Left => input_control.set_moving_x(-1. * key_down),
                Keycode::Right => input_control.set_moving_x(1. * key_down),
                Keycode::Return => input_control.build = key_down_bool,
                Keycode::RShift => input_control.destroy = key_down_bool,
                Keycode::PageUp => input_control.prev_tile = key_down_bool,
                Keycode::PageDown => input_control.next_tile = key_down_bool,
                Keycode::RCtrl => input_control.shoot = key_down_bool,
                _ => (),
            }
        }
    }

    fn manage_controller(&self, input_control: &mut cmn::components::InputControl) {
        if let (Some(btn), Some(key_down)) = (self.btn, self.key_down) {
            match btn {
                Button::DPadUp => input_control.prev_projectile = key_down,
                Button::DPadDown => input_control.next_projectile = key_down,
                Button::DPadLeft => input_control.prev_tile = key_down,
                Button::DPadRight => input_control.next_tile = key_down,
                Button::RightShoulder => input_control.build = key_down,
                Button::LeftShoulder => input_control.destroy = key_down,
                Button::A => input_control.prev_tile = key_down,
                Button::B => input_control.next_tile = key_down,
                Button::X => input_control.shoot = key_down,
                _ => (),
            }
        } else if let Some((axis, value)) = self.axis {
            let key_down = if value < 1. { false } else { true };

            match axis {
                Axis::TriggerRight => input_control.shoot = key_down,
                Axis::RightX => input_control.set_aiming_x(value),
                Axis::RightY => input_control.set_aiming_y(value),
                Axis::LeftX => input_control.set_moving_x(value),
                Axis::LeftY => input_control.set_moving_y(value),
                _ => (),
            }
        }
    }
}

impl<'a> System<'a> for InputController {
    type SystemData = (
        ReadStorage<'a, cmn::components::PlayerProfile>,
        WriteStorage<'a, cmn::components::InputControl>,
    );

    fn run(&mut self, (player_profile_strg, mut input_control_strg): Self::SystemData) {
        for (player_profile, input_control) in
            (&player_profile_strg, &mut input_control_strg).join()
        {
            match (player_profile, self.instance_id) {
                (PlayerProfile::One, Some(0)) => self.manage_controller(input_control),
                (PlayerProfile::One, None) => self.manage_keyboard_one(input_control),
                (PlayerProfile::Two, Some(1)) => self.manage_controller(input_control),
                (PlayerProfile::Two, None) => self.manage_keyboard_two(input_control),
                (_, _) => (),
            }
        }
    }
}
