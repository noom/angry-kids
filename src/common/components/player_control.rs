use specs::prelude::*;
use specs_derive::Component;

use nalgebra::Real;

use crate::consts::*;

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct Controllable;

#[derive(Component, PartialEq, Clone, Debug)]
pub enum PlayerProfile {
    One,
    Two,
}

#[derive(Component, Default)]
pub struct InputControl {
    pub up: bool,
    pub down: bool,
    pub left: bool,
    pub right: bool,
    pub build: bool,
    pub destroy: bool,
    pub next_tile: bool,
    pub prev_tile: bool,
    pub next_projectile: bool,
    pub prev_projectile: bool,
    pub shoot: bool,

    moving: f32,
    moving_x: f32,
    moving_y: f32,

    aiming: f32,
    aiming_x: f32,
    aiming_y: f32,
}

impl InputControl {
    pub fn jump(&self) -> bool {
        let dist_from_zero = self.moving_x.powi(2) + self.moving_y.powi(2);
        if dist_from_zero < JOYSTICK_THRESHOLD {
            return false;
        }

        self.moving >= -(5. * f32::pi() / 6.) && self.moving <= -(f32::pi() / 6.)
    }

    pub fn move_down(&self) -> bool {
        let dist_from_zero = self.moving_x.powi(2) + self.moving_y.powi(2);
        if dist_from_zero < JOYSTICK_THRESHOLD {
            return false;
        }

        self.moving >= f32::pi() / 6. && self.moving <= 5. * f32::pi() / 6.
    }

    pub fn move_left(&self) -> bool {
        let dist_from_zero = self.moving_x.powi(2) + self.moving_y.powi(2);
        if dist_from_zero < JOYSTICK_THRESHOLD {
            return false;
        }

        self.moving >= 2. * f32::pi() / 3. && self.moving <= f32::pi()
            || self.moving <= -(2. * f32::pi() / 3.) && self.moving >= -f32::pi()
    }

    pub fn move_right(&self) -> bool {
        let dist_from_zero = self.moving_x.powi(2) + self.moving_y.powi(2);
        if dist_from_zero < JOYSTICK_THRESHOLD {
            return false;
        }

        self.moving >= -(f32::pi() / 3.) && self.moving <= f32::pi() / 3.
    }

    pub fn set_moving_x(&mut self, moving_x: f32) {
        self.moving_x = moving_x;

        self.moving = self.moving_y.atan2(self.moving_x);
    }

    pub fn set_moving_y(&mut self, moving_y: f32) {
        self.moving_y = moving_y;

        self.moving = self.moving_y.atan2(self.moving_x);
    }

    pub fn aiming(&self) -> f32 {
        self.aiming
    }

    pub fn aiming_x(&self) -> f32 {
        self.aiming.cos()
    }

    pub fn aiming_y(&self) -> f32 {
        self.aiming.sin()
    }

    pub fn set_aiming_x(&mut self, aiming_x: f32) {
        self.aiming_x = aiming_x;

        let dist_from_zero = self.aiming_x.powi(2) + self.aiming_y.powi(2);
        if dist_from_zero > JOYSTICK_THRESHOLD {
            self.aiming = self.aiming_y.atan2(self.aiming_x);
        }
    }

    pub fn set_aiming_y(&mut self, aiming_y: f32) {
        self.aiming_y = aiming_y;

        let dist_from_zero = self.aiming_x.powi(2) + self.aiming_y.powi(2);
        if dist_from_zero > JOYSTICK_THRESHOLD {
            self.aiming = self.aiming_y.atan2(self.aiming_x);
        }
    }
}
