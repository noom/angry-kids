use specs::prelude::*;
use specs_derive::Component;

use crate::assets::AssetKey;

#[derive(Component)]
pub struct Image(AssetKey, Option<u32>);

impl Image {
    pub fn new(asset_key: AssetKey) -> Self {
        Self(asset_key, None)
    }

    pub fn with_tile_number(self, tile: u32) -> Self {
        Self(self.0, Some(tile))
    }

    pub fn set_tile_number(&mut self, tile: u32) {
        self.1 = Some(tile);
    }

    pub fn asset_key(&self) -> &AssetKey {
        &self.0
    }

    pub fn tile_number(&self) -> Option<u32> {
        self.1
    }
}

#[derive(Component)]
pub struct Animable(u32, u32, u32);

impl Animable {
    pub fn new(tile_start: u32, nb_steps: u32) -> Self {
        Self(tile_start, nb_steps, tile_start)
    }

    pub fn step(&mut self) {
        self.2 = (self.2 - self.0 + 1) % self.1 + self.0;
    }

    pub fn animation_frame(&self) -> u32 {
        self.2
    }
}

impl PartialEq for Animable {
    fn eq(&self, other: &Self) -> bool {
        other.0 == self.0 && other.1 == self.1
    }
}
