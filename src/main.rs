use std::{env, path};

use ggez::{conf, event, Context};

mod assets;
mod consts;
mod game;
mod game_manager;
mod menu;
mod common;

use crate::{assets::Assets, consts::*, game_manager::GameManager};

pub fn main() {
    let window_mode = conf::WindowMode::default()
        .dimensions(SCREEN_WH.0, SCREEN_WH.1)
        .fullscreen_type(conf::FullscreenType::Off);

    let window_setup = conf::WindowSetup::default().title("Angry Kids");

    let conf = conf::Conf {
        window_mode,
        window_setup,
        ..Default::default()
    };

    let ctx = &mut Context::load_from_conf("Angry Kids", "Lancelot et Eyal", conf).unwrap();

    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("assets");
        ctx.filesystem.mount(&path, true);
    }

    let state = &mut GameManager::new(ctx).unwrap();
    event::run(ctx, state).unwrap();
}
