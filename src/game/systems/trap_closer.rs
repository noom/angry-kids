use specs::prelude::*;

use nphysics2d::{object::ColliderHandle, world::World as NphysicsWorld};

use crate::{
    common as cmn,
    game::{
        components, entities,
        resources::{self, TileKey},
    },
};

// TODO: Unify this with TrapOpener
pub struct TrapCloser;

impl TrapCloser {
    pub fn is_player_colliding(
        world: &NphysicsWorld<f32>,
        tile_collider: ColliderHandle,
        player_collider: ColliderHandle,
    ) -> bool {
        world
            .collider_world()
            .proximity_pair(player_collider, tile_collider, true)
            .is_some()
    }
}

impl<'a> System<'a> for TrapCloser {
    type SystemData = (
        Entities<'a>,
        Read<'a, LazyUpdate>,
        Write<'a, resources::Map>,
        Read<'a, resources::Tiles>,
        Write<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::Tile>,
        ReadStorage<'a, components::TileActionTrapClose>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, cmn::components::PlayerProfile>,
    );

    fn run(
        &mut self,
        (
            entities,
            builder,
            mut map_res,
            tiles_res,
            mut world_res,
            tile_strg,
            tile_action_trap_strg,
            collider_handle_strg,
            player_profile_strg,
        ): Self::SystemData,
    ) {
        let traps: Vec<(Entity, ColliderHandle, &components::Tile)> = (
            &tile_action_trap_strg,
            &entities,
            &collider_handle_strg,
            &tile_strg,
        )
            .join()
            .map(|(_, entity, collider_handle, tile)| (entity, collider_handle.handle(), tile))
            .collect();

        if traps.len() == 0 {
            return;
        }

        let players_collider: Vec<ColliderHandle> = (&player_profile_strg, &collider_handle_strg)
            .join()
            .map(|(_, player_collider)| player_collider.handle())
            .collect();

        let traps_to_remove: Vec<(Entity, ColliderHandle, &components::Tile)> = traps
            .iter()
            .cloned()
            .filter(|(_, trap_collider, _)| {
                let mut colliding = false;

                for player_collider in players_collider.iter() {
                    if Self::is_player_colliding(
                        world_res.world(),
                        *trap_collider,
                        *player_collider,
                    ) {
                        colliding = true;
                        break;
                    }
                }

                !colliding
            })
            .collect();

        for (trap_entity, trap_collider, trap_tile) in traps_to_remove.iter() {
            let (tile_pos_x, tile_pos_y) = {
                let collider = world_res.world().collider(*trap_collider).unwrap();
                let pos = collider.position().translation.vector;
                (pos.x.floor() as u32, (-pos.y).floor() as u32)
            };

            entities
                .delete(*trap_entity)
                .expect(format!("Couldnt remove trap_entity {:?}", trap_entity).as_str());
            world_res.world_mut().remove_colliders(&[*trap_collider]);

            let new_entity = entities::create_tile(
                builder.create_entity(&entities),
                trap_tile.built_by().clone(),
            );
            map_res.update_entity_at_xy(new_entity, tile_pos_x as i32, tile_pos_y as i32);

            entities::make_tile(
                &builder,
                &tiles_res,
                &TileKey::TrapClosed,
                new_entity,
                tile_pos_x,
                tile_pos_y,
                0.,
            );
        }
    }
}
