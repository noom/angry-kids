use specs::prelude::*;

use nalgebra::{Isometry2, Vector2};

use crate::{consts::*, game::{components, entities::ColliderTag, resources}};

pub struct ProjectileRotation;

impl<'a> System<'a> for ProjectileRotation {
    type SystemData = (
        Write<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::Projectile>,
        ReadStorage<'a, components::ProjectileRotation>,
        WriteStorage<'a, components::RigidBodyHandle>,
    );

    fn run(
        &mut self,
        (mut world_res, projectile_strg, projectile_rotation_strg, mut rigid_body_handle_strg): Self::SystemData,
    ) {
        for (_, _, rigid_body_handle) in (
            &projectile_strg,
            &projectile_rotation_strg,
            &mut rigid_body_handle_strg,
        )
            .join()
        {
            let rigid_body = world_res
                .world_mut()
                .rigid_body_mut(rigid_body_handle.handle())
                .unwrap();

            let velocity = rigid_body.velocity();
            let rotation = velocity.linear.x.atan2(velocity.linear.y);
            let position = rigid_body.position().clone();
            rigid_body.set_position(Isometry2::new(position.translation.vector, rotation));
        }
    }
}

pub struct ProjectileCollision;

impl<'a> System<'a> for ProjectileCollision {
    type SystemData = (
        Entities<'a>,
        Write<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::Projectile>,
        ReadStorage<'a, components::ProjectileActionDamage>,
        WriteStorage<'a, components::RigidBodyHandle>,
        WriteStorage<'a, components::ColliderHandle>,
        WriteStorage<'a, components::Remanent>,
        WriteStorage<'a, components::Clock>,
        WriteStorage<'a, components::Life>,
        WriteStorage<'a, components::Damaged>,
    );

    fn run(
        &mut self,
        (
            entities,
            mut world_res,
            projectile_strg,
            projectile_action_damage_strg,
            mut rigid_body_handle_strg,
            mut collider_handle_strg,
            mut remanent_strg,
            mut clock_strg,
            mut life_strg,
            mut damaged_strg,
        ): Self::SystemData,
    ) {
        let mut entities_to_wipe: Vec<Entity> = Vec::new();
        let mut collider_in_contact = Vec::new();

        for (entity, _, damage, rigid_body_handle, collider_handle, clock) in (
            &entities,
            &projectile_strg,
            &projectile_action_damage_strg,
            &mut rigid_body_handle_strg,
            &collider_handle_strg,
            &mut clock_strg,
        )
            .join()
        {
            let rigid_body = world_res
                .world()
                .rigid_body(rigid_body_handle.handle())
                .unwrap();

            let contact_collider = {
                let mut contacts = world_res
                    .world()
                    .collider_world()
                    .contacts_with(collider_handle.handle(), true)
                    .unwrap();

                if let Some(contact) = contacts.next() {
                    contact.1
                } else {
                    continue;
                }
            };

            collider_in_contact.push((contact_collider.handle(), damage.0));
            if let Some(user_data) = contact_collider.user_data() {
                if let Some(extracted_data) = user_data.downcast_ref::<ColliderTag>() {
                    if extracted_data == &ColliderTag::ForceCollision {
                        continue;
                    }
                }
            }

            let projectile_pos = rigid_body.position().clone();
            let projectile_vec = projectile_pos.translation.vector;
            let projectile_rot = projectile_pos.rotation.angle();

            let contact_vec = contact_collider.position().translation.vector;

            let rel_vec = Vector2::new(
                contact_vec.x - projectile_vec.x,
                contact_vec.y - projectile_vec.y,
            );
            let rel_pos = Isometry2::new(rel_vec, projectile_rot);

            let remanent = components::Remanent::new(rel_pos, contact_collider.handle());
            remanent_strg
                .insert(entity, remanent)
                .expect("Could not insert Remanent component");

            clock.add_timer(components::TimerId::RemoveEntity, REMANENT_PROJECTILE_TIMER);

            world_res
                .world_mut()
                .remove_colliders(&[collider_handle.handle()]);
            world_res
                .world_mut()
                .remove_bodies(&[rigid_body_handle.handle()]);

            entities_to_wipe.push(entity);
        }

        for entity in entities_to_wipe.iter() {
            rigid_body_handle_strg.remove(*entity);
            collider_handle_strg.remove(*entity);
        }

        for (entity, collider_handle, life, clock) in (
            &entities,
            &collider_handle_strg,
            &mut life_strg,
            &mut clock_strg,
        )
            .join()
        {
            if damaged_strg.contains(entity) || life.life() == 0. {
                continue;
            }

            for (contact_collider_handle, damage) in collider_in_contact.iter() {
                if &collider_handle.handle() == contact_collider_handle {
                    life.change_by(-1. * *damage as f32);

                    damaged_strg
                        .insert(entity, components::Damaged::default())
                        .expect("Couldnt insert Damaged Component");

                    clock.add_timer(components::TimerId::RemoveDamaged, DAMAGED_DURATION);
                }
            }
        }
    }
}
