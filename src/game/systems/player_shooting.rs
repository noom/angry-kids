use specs::prelude::*;

use nalgebra::Vector2;

use crate::{common as cmn, game::{
    components, entities,
    resources::{self, ProjectileKey},
}};

pub struct PlayerShooting;

impl<'a> System<'a> for PlayerShooting {
    type SystemData = (
        Entities<'a>,
        Read<'a, LazyUpdate>,
        Write<'a, resources::PhysicsWorld>,
        Read<'a, resources::Projectiles>,
        ReadStorage<'a, cmn::components::Controllable>,
        ReadStorage<'a, components::Selector<ProjectileKey>>,
        ReadStorage<'a, components::ColliderHandle>,
        WriteStorage<'a, cmn::components::InputControl>,
    );

    fn run(
        &mut self,
        (
            entities,
            builder,
            mut world_res,
            projectiles_res,
            controllable_strg,
            projectile_selector_strg,
            collider_handle_strg,
            mut input_control_strg,
        ): Self::SystemData,
    ) {
        for (_, projectile_selector, collider_handle, mut input_control) in (
            &controllable_strg,
            &projectile_selector_strg,
            &collider_handle_strg,
            &mut input_control_strg,
        )
            .join()
        {
            if !input_control.shoot {
                continue;
            }

            input_control.shoot = false;

            let collider = world_res
                .world()
                .collider(collider_handle.handle())
                .unwrap();
            let collider_pos = collider.position().translation.vector;

            let aiming_x = input_control.aiming_x();
            let aiming_y = input_control.aiming_y();

            let aiming = Vector2::new(aiming_x, -aiming_y);

            entities::create_projectile(
                &builder,
                &entities,
                &projectiles_res,
                &mut world_res,
                projectile_selector.selected().clone(),
                collider_pos,
                aiming,
            );
        }
    }
}
