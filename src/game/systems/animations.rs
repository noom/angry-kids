use specs::prelude::*;

use crate::{
    common as cmn,
    consts::*,
    game::{components, components::Direction::*, resources},
};

pub struct Animations;

impl<'a> System<'a> for Animations {
    type SystemData = (
        Write<'a, resources::AnimationTime>,
        WriteStorage<'a, cmn::components::Animable>,
        WriteStorage<'a, cmn::components::Image>,
        ReadStorage<'a, components::FacingDirection>,
        ReadStorage<'a, cmn::components::InputControl>,
    );

    fn run(
        &mut self,
        (
            mut time,
            mut animable_strg,
            mut image_strg,
            facing_direction_strg,
            input_control_strg,
        ): Self::SystemData,
    ) {
        time.step();

        for (animable, image) in (&mut animable_strg, &mut image_strg).join() {
            image.set_tile_number(animable.animation_frame());
            if time.value() % (0.1 * FPS as f32) as u32 == 0 {
                animable.step();
            }
        }

        for (animable, facing_direction, input_control) in (
            &mut animable_strg,
            &facing_direction_strg,
            &input_control_strg,
        )
            .join()
        {
            let animation = match (
                input_control.move_left() ^ input_control.move_right(),
                facing_direction.direction,
            ) {
                (true, Left) => cmn::components::Animable::new(8, 4),
                (true, Right) => cmn::components::Animable::new(2, 4),
                (false, Left) => cmn::components::Animable::new(6, 1),
                (false, Right) => cmn::components::Animable::new(0, 1),
            };

            if *animable != animation {
                *animable = animation;
            }
        }
    }
}
