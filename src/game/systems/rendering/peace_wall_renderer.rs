use std::ops::Deref;

use ggez::{
    graphics::{draw_ex, rectangle, set_color, Color, DrawMode, DrawParam, Point2, Rect},
    Context,
};

use specs::prelude::*;

use nalgebra::Vector2;

use crate::{consts::*, game::{
    components::{self, TimerId},
    resources,
}};

pub struct PeaceWallRenderer<'a> {
    ctx: &'a mut Context,
}

impl<'a> PeaceWallRenderer<'a> {
    pub fn new(ctx: &'a mut Context) -> Self {
        Self { ctx }
    }
}

impl<'a> System<'a> for PeaceWallRenderer<'a> {
    type SystemData = (
        Read<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, components::Clock>,
        ReadStorage<'a, components::PeaceWall>,
    );

    fn run(
        &mut self,
        (world_res, collider_handle_strg, clock_strg, peace_wall_strg): Self::SystemData,
    ) {
        // TODO: Use the clock to make it slowly fading?
        for (_, collider_handle, clock) in
            (&peace_wall_strg, &collider_handle_strg, &clock_strg).join()
        {
            let position = world_res
                .world()
                .collider(collider_handle.handle())
                .unwrap()
                .position()
                .translation
                .vector;

            let rect = Rect::new(
                (position.x - (MAP_WH.0 as f32 / 3. / 2.)) * SCALE_FACTOR,
                0.,
                // TODO: Should get the size from the shape
                MAP_WH.0 as f32 / 3. * SCALE_FACTOR,
                1000.,
            );

            set_color(self.ctx, Color::new(1., 0., 0., 0.25)).expect("Couldn't set the color");
            rectangle(self.ctx, DrawMode::Fill, rect).expect("Couldn't draw");
            set_color(self.ctx, Color::new(1., 1., 1., 1.)).expect("Couldn't set the color");
        }
    }
}
