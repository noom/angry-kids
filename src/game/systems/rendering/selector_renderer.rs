use std::ops::Deref;

use ggez::{
    graphics::{draw_ex, rectangle, set_color, Color, DrawMode, DrawParam, Point2, Rect},
    Context,
};

use specs::prelude::*;

use crate::{
    assets::{Assets, Selectable},
    consts::*,
    common as cmn,
    game::{
        components::{self, DisplaySelectorState, TimerId},
        resources::{self, ProjectileKey, TileKey},
    },
};

pub struct SelectorRenderer<'a> {
    ctx: &'a mut Context,
    assets: &'a Assets,
}

impl<'a> SelectorRenderer<'a> {
    pub fn new(ctx: &'a mut Context, assets: &'a Assets) -> Self {
        Self { ctx, assets }
    }

    fn get_draw_elems_params<T: Send + Sync>(
        &mut self,
        world_res: &resources::PhysicsWorld,
        clock: &components::Clock,
        collider_handle: &components::ColliderHandle,
        selector: &components::Selector<T>,
        timer_id: &TimerId,
    ) -> Option<(f32, f32, f32, i32, usize, usize, f32)> {
        if *selector.state() == DisplaySelectorState::Invisible {
            return None;
        }

        let elems_max = selector.objects().len();
        let elems_count: usize = 3;
        let start = selector.selection_index() as i32 - (elems_count / 2) as i32;
        let gap_between = 3.5;
        let selector_full_len =
            elems_count as f32 * SCALE_FACTOR + gap_between * elems_count as f32 - gap_between;

        let collider = world_res
            .world()
            .collider(collider_handle.handle())
            .unwrap();
        let player_pos = collider.position().translation.vector;

        let x = player_pos.x * SCALE_FACTOR - selector_full_len / 2.;
        let y = -player_pos.y * SCALE_FACTOR - SCALE_FACTOR * 1.75;

        let transparency = match selector.state() {
            DisplaySelectorState::FadingIn => clock
                .progress_perc(timer_id)
                .expect("FadingIn selector does not have timer"),
            DisplaySelectorState::FadingOut => {
                1. - clock
                    .progress_perc(timer_id)
                    .expect("FadingOut selector does not have timer")
            }
            DisplaySelectorState::Visible => 1.,
            _ => 0.,
        };

        Some((
            x,
            y,
            gap_between,
            start,
            elems_count,
            elems_max,
            transparency,
        ))
    }

    fn draw_elems<T: Send + Sync, O: Selectable<T>>(
        &mut self,
        object_res: &O,
        selector: &components::Selector<T>,
        x: f32,
        y: f32,
        gap_between: f32,
        start: i32,
        elems_count: usize,
        elems_max: usize,
        transparency: f32,
    ) {
        let begin = (start % elems_max as i32 + elems_max as i32) % elems_max as i32;
        let objects = selector.objects();

        let rect_margin = 5.;

        let rect = Rect::new(
            x - rect_margin,
            y - rect_margin,
            elems_count as f32 * SCALE_FACTOR
                + (gap_between * elems_count as f32 - gap_between)
                + rect_margin * 2.,
            SCALE_FACTOR + rect_margin * 2.,
        );

        set_color(self.ctx, Color::new(0., 0., 0., transparency / 2.))
            .expect("Couldn't set the color");
        rectangle(self.ctx, DrawMode::Fill, rect).expect("Couldn't draw");
        set_color(self.ctx, Color::new(1., 1., 1., 1.)).expect("Couldn't set the color");

        for i in 0..elems_count {
            let object = (begin + i as i32) % elems_max as i32;
            let object_key = &objects[object as usize];

            let asset_key = object_res.asset_key_for(object_key);

            let texture = self.assets.texture_for(&asset_key);
            let image_drawable = Deref::deref(texture.image());

            let (texture_width, texture_height) = texture.texture_size();
            let (image_scale_x, image_scale_y) = (
                SCALE_FACTOR / texture_width as f32,
                SCALE_FACTOR / texture_height as f32,
            );

            let transparency = if object as usize == selector.selection_index() {
                transparency * SELECTOR_TRANSPARENCY
            } else {
                transparency * SELECTOR_TRANSPARENCY * 0.1
            };

            draw_ex(
                self.ctx,
                image_drawable,
                DrawParam {
                    dest: Point2::new(x + i as f32 * SCALE_FACTOR + gap_between * i as f32, y),
                    scale: Point2::new(image_scale_x, image_scale_y),
                    color: Some(Color::new(1., 1., 1., transparency)),
                    ..self.assets.get_draw_params(&asset_key, None)
                },
            )
            .expect("couldn't draw");
        }
    }
}

impl<'a> System<'a> for SelectorRenderer<'a> {
    type SystemData = (
        Read<'a, resources::PhysicsWorld>,
        Read<'a, resources::Tiles>,
        Read<'a, resources::Projectiles>,
        ReadStorage<'a, components::Selector<TileKey>>,
        ReadStorage<'a, components::Selector<ProjectileKey>>,
        ReadStorage<'a, cmn::components::PlayerProfile>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, components::Clock>,
    );

    fn run(
        &mut self,
        (
            world_res,
            tiles_res,
            projectiles_res,
            tile_selector_strg,
            projectile_selector_strg,
            player_profile_strg,
            collider_handle_strg,
            clock_strg,
        ): Self::SystemData,
    ) {
        for (_, clock, collider_handle, tile_selector, projectile_selector) in (
            &player_profile_strg,
            &clock_strg,
            &collider_handle_strg,
            &tile_selector_strg,
            &projectile_selector_strg,
        )
            .join()
        {
            if let Some((x, y, gap_between, start, elems_count, elems_max, transparency)) = self
                .get_draw_elems_params(
                    &world_res,
                    clock,
                    collider_handle,
                    tile_selector,
                    &TimerId::DisplayTileSelector,
                )
            {
                self.draw_elems(
                    tiles_res.deref(),
                    &tile_selector,
                    x,
                    y,
                    gap_between,
                    start,
                    elems_count,
                    elems_max,
                    transparency,
                );
            }

            if let Some((x, y, gap_between, start, elems_count, elems_max, transparency)) = self
                .get_draw_elems_params(
                    &world_res,
                    clock,
                    collider_handle,
                    projectile_selector,
                    &TimerId::DisplayProjectileSelector,
                )
            {
                self.draw_elems(
                    projectiles_res.deref(),
                    &projectile_selector,
                    x,
                    y,
                    gap_between,
                    start,
                    elems_count,
                    elems_max,
                    transparency,
                );
            }
        }
    }
}
