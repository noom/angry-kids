use std::ops::Deref;

use ggez::{
    graphics::{draw_ex, Color, DrawParam, Point2},
    Context,
};

use specs::prelude::*;

use nalgebra::Vector2;

use crate::{
    assets::Assets,
    common as cmn,
    consts::*,
    game::{
        components::{self, TimerId},
        resources,
    },
};

pub struct EntityRenderer<'a> {
    ctx: &'a mut Context,
    assets: &'a Assets,
}

impl<'a> EntityRenderer<'a> {
    pub fn new(ctx: &'a mut Context, assets: &'a Assets) -> Self {
        Self { ctx, assets }
    }
}

impl<'a> System<'a> for EntityRenderer<'a> {
    type SystemData = (
        Read<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, components::Remanent>,
        ReadStorage<'a, components::Construction>,
        ReadStorage<'a, components::Destruction>,
        ReadStorage<'a, components::Damaged>,
        ReadStorage<'a, components::Clock>,
        ReadStorage<'a, cmn::components::Image>,
    );

    fn run(
        &mut self,
        (
            world_res,
            collider_handle_strg,
            remanent_strg,
            construction_strg,
            destruction_strg,
            damaged_strg,
            clock_strg,
            image_strg,
        ): Self::SystemData,
    ) {
        for (collider_handle, remanent, construction, destruction, damaged, clock, image) in (
            collider_handle_strg.maybe(),
            remanent_strg.maybe(),
            construction_strg.maybe(),
            destruction_strg.maybe(),
            damaged_strg.maybe(),
            clock_strg.maybe(),
            &image_strg,
        )
            .join()
        {
            let (position, rotation, transparency) = {
                if let (Some(destruction), Some(clock)) = (destruction, clock) {
                    let timer = clock
                        .timer(&TimerId::DestructingTile)
                        .expect("Tile in destruction doesn't have a timer");

                    let collider = world_res
                        .world()
                        .collider(*destruction.collider_handle())
                        .unwrap();

                    let pos = collider.position().translation.vector;

                    let perc = 1. - timer.progress_perc();

                    (pos, 0., perc)
                } else if let (Some(construction), Some(clock)) = (construction, clock) {
                    let timer = clock
                        .timer(&TimerId::ConstructingTile)
                        .expect("Tile in construction doesn't have a timer");

                    let collider_desc = construction.collider_desc();
                    let pos = collider_desc.get_position().translation.vector;

                    (pos, 0., timer.progress_perc())
                } else if let Some(collider_handle) = collider_handle {
                    let collider = world_res
                        .world()
                        .collider(collider_handle.handle())
                        .unwrap();

                    let transparency = if let (Some(_), Some(clock)) = (damaged, clock) {
                        let timer = clock
                            .timer(&TimerId::RemoveDamaged)
                            .expect("Damaged entity doesn't have a timer");

                        let blink_per_sec = 6.;
                        (timer.time_left() % (1. / blink_per_sec)) * blink_per_sec
                    } else {
                        1.
                    };

                    (
                        collider.position().translation.vector,
                        collider.position().rotation.angle(),
                        transparency,
                    )
                } else if let Some(remanent) = remanent {
                    let resting_on = world_res
                        .world()
                        .collider(remanent.resting_on_handle())
                        .expect("Could not get remanent's resting_on collider");

                    let resting_on_pos = resting_on.position().translation.vector;

                    let pos = Vector2::new(
                        resting_on_pos.x - remanent.rel_pos().translation.x,
                        resting_on_pos.y - remanent.rel_pos().translation.y,
                    );

                    let transparency = if let Some(clock) = clock {
                        let timer = clock
                            .timer(&TimerId::RemoveEntity)
                            .expect("Remanent doesn't have a timer");

                        let transparency = 1. - timer.progress() / timer.duration();

                        if transparency > 0.125 {
                            1.
                        } else {
                            transparency * 8.
                        }
                    } else {
                        1.
                    };

                    (pos, remanent.rel_pos().rotation.angle(), transparency)
                } else {
                    continue;
                }
            };

            let texture = self.assets.texture_for(image.asset_key());
            let image_drawable = Deref::deref(texture.image());

            let (texture_width, texture_height) = texture.texture_size();
            let (image_scale_x, image_scale_y) = (
                SCALE_FACTOR / texture_width as f32,
                SCALE_FACTOR / texture_height as f32,
            );

            draw_ex(
                self.ctx,
                image_drawable,
                DrawParam {
                    dest: Point2::new(position.x * SCALE_FACTOR, -position.y * SCALE_FACTOR),
                    offset: Point2::new(0.5, 0.5),
                    scale: Point2::new(image_scale_x, image_scale_y),
                    color: Some(Color::new(1., 1., 1., transparency)),
                    rotation,
                    ..self
                        .assets
                        .get_draw_params(image.asset_key(), image.tile_number())
                },
            )
            .expect("couldn't draw");
        }
    }
}
