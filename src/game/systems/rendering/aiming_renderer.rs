use specs::prelude::*;

use ggez::{
    graphics::{circle, DrawMode, Point2},
    Context,
};

use crate::{consts::*, common as cmn, game::{components, resources}};

pub struct AimingRenderer<'a> {
    ctx: &'a mut Context,
}

impl<'a> AimingRenderer<'a> {
    pub fn new(ctx: &'a mut Context) -> Self {
        Self { ctx }
    }
}

impl<'a> System<'a> for AimingRenderer<'a> {
    type SystemData = (
        Read<'a, resources::PhysicsWorld>,
        ReadStorage<'a, cmn::components::InputControl>,
        ReadStorage<'a, components::ColliderHandle>,
    );

    fn run(&mut self, (world_res, input_control_strg, collider_handle_strg): Self::SystemData) {
        for (input_control, collider_handle) in (&input_control_strg, &collider_handle_strg).join()
        {
            let collider = world_res
                .world()
                .collider(collider_handle.handle())
                .unwrap();
            let collider_pos = collider.position().translation.vector;

            let pos = Point2::new(
                (collider_pos.x + input_control.aiming_x()) * SCALE_FACTOR,
                (-collider_pos.y + input_control.aiming_y()) * SCALE_FACTOR,
            );

            circle(self.ctx, DrawMode::Fill, pos, 5., 0.01).unwrap();
        }
    }
}
