use ggez::{
    graphics::{rectangle, DrawMode, Rect},
    Context,
};

use specs::prelude::*;

use crate::{game::components, consts::*};

pub struct FacingTileRenderer<'a> {
    ctx: &'a mut Context,
}

impl<'a> FacingTileRenderer<'a> {
    pub fn new(ctx: &'a mut Context) -> Self {
        Self { ctx }
    }
}

impl<'a> System<'a> for FacingTileRenderer<'a> {
    type SystemData = ReadStorage<'a, components::FacingTile>;

    fn run(&mut self, facing_tile_strg: Self::SystemData) {
        for (facing_tile,) in (&facing_tile_strg,).join() {
            let rect = Rect::new(
                facing_tile.x as f32 * SCALE_FACTOR,
                facing_tile.y as f32 * SCALE_FACTOR,
                SCALE_FACTOR,
                SCALE_FACTOR,
            );

            rectangle(self.ctx, DrawMode::Line(2.), rect).expect("Couldn't draw");
        }
    }
}
