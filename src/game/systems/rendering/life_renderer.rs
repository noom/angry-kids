use std::ops::Deref;

use ggez::{
    graphics::{draw_ex, DrawParam, Point2},
    Context,
};

use specs::prelude::*;

use crate::{
    assets::{AssetKey, Assets},
    common::{self as cmn, PlayerProfile},
    consts::*,
    game::{components, resources},
};

pub struct LifeRenderer<'a> {
    ctx: &'a mut Context,
    assets: &'a Assets,
}

impl<'a> LifeRenderer<'a> {
    pub fn new(ctx: &'a mut Context, assets: &'a Assets) -> Self {
        Self { ctx, assets }
    }
}

impl<'a> System<'a> for LifeRenderer<'a> {
    type SystemData = (
        ReadStorage<'a, cmn::components::PlayerProfile>,
        ReadStorage<'a, components::Life>,
    );

    fn run(&mut self, (player_profile_strg, life_strg): Self::SystemData) {
        for (player_profile, life) in (&player_profile_strg, &life_strg).join() {
            let y = 5.;
            let x = match player_profile {
                PlayerProfile::One => 20.,
                PlayerProfile::Two => {
                    SCREEN_WH.0 as f32
                        - (20. + (LIFE_AMOUNT as i32 / 2) as f32 * ((SCALE_FACTOR / 2.) + 3.))
                }
            };

            for i in 0..LIFE_AMOUNT as i32 / 2 {
                let texture = self.assets.texture_for(&AssetKey::Heart);
                let image_drawable = Deref::deref(texture.image());

                let texture_tile = if life.life() as i32 - i * 2 >= 2 {
                    0
                } else if life.life() as i32 - i * 2 == 1 {
                    1
                } else {
                    2
                };

                let (texture_width, texture_height) = texture.texture_size();
                let (image_scale_x, image_scale_y) = (
                    SCALE_FACTOR / (texture_width as f32 * 2.),
                    SCALE_FACTOR / (texture_height as f32 * 2.),
                );

                draw_ex(
                    self.ctx,
                    image_drawable,
                    DrawParam {
                        dest: Point2::new(x + i as f32 * (SCALE_FACTOR / 2.) + 3. * i as f32, y),
                        scale: Point2::new(image_scale_x, image_scale_y),
                        ..self
                            .assets
                            .get_draw_params(&AssetKey::Heart, Some(texture_tile))
                    },
                )
                .expect("couldn't draw");
            }
        }
    }
}
