use specs::prelude::*;

use nphysics2d::{object::ColliderHandle, world::World as NphysicsWorld};

use crate::{consts::*, game::{
    components::{self, TimerId},
    entities::ColliderTag,
    resources,
}};

pub struct TileDamageDealer;

impl TileDamageDealer {
    fn is_player_on_spikes(
        world: &NphysicsWorld<f32>,
        map: &Read<'_, resources::Map>,
        tile_action_damage_strg: &ReadStorage<'_, components::TileActionDealDamage>,
        player: ColliderHandle,
    ) -> bool {
        for proximity in world
            .collider_world()
            .proximities_with(player, true)
            .expect("player collider does not exists")
        {
            if let Some(user_data) = proximity.1.user_data() {
                if let Some(extracted_data) = user_data.downcast_ref::<ColliderTag>() {
                    if extracted_data == &ColliderTag::IgnoreCollision {
                        let pos = proximity.1.position().translation.vector;
                        let tile_entity = map
                            .entity_at_xy(pos.x.floor() as i32, (-pos.y).floor() as i32)
                            .expect("no tile entity there");

                        if tile_action_damage_strg.contains(tile_entity) {
                            return true;
                        }
                    }
                }

                return false;
            } else {
                return false;
            }
        }

        false
    }
}

impl<'a> System<'a> for TileDamageDealer {
    type SystemData = (
        Entities<'a>,
        Read<'a, resources::Map>,
        Read<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::TileActionDealDamage>,
        ReadStorage<'a, components::ColliderHandle>,
        WriteStorage<'a, components::Life>,
        WriteStorage<'a, components::Damaged>,
        WriteStorage<'a, components::Clock>,
    );

    fn run(
        &mut self,
        (
            entities,
            map_res,
            world_res,
            tile_action_damage_strg,
            collider_handle_strg,
            mut life_strg,
            mut damaged_strg,
            mut clock_strg,
        ): Self::SystemData,
    ) {
        for (entity, life, collider_handle, clock) in (
            &entities,
            &mut life_strg,
            &collider_handle_strg,
            &mut clock_strg,
        )
            .join()
        {
            if life.life() > 0.
                && !damaged_strg.contains(entity)
                && Self::is_player_on_spikes(
                    world_res.world(),
                    &map_res,
                    &tile_action_damage_strg,
                    collider_handle.handle(),
                )
            {
                life.change_by(-2.);

                damaged_strg
                    .insert(entity, components::Damaged::default())
                    .expect("Couldnt insert Damaged Component");

                clock.add_timer(TimerId::RemoveDamaged, DAMAGED_DURATION);
            }
        }
    }
}
