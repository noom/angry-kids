use specs::prelude::*;

use nphysics2d::{object::ColliderHandle, world::World as NphysicsWorld};

use crate::{
    common::{self as cmn, PlayerProfile},
    game::{
        components, entities,
        resources::{self, TileKey},
    },
};

pub struct TrapOpener;

impl TrapOpener {
    pub fn is_player_on_top(
        world: &NphysicsWorld<f32>,
        trap_collider: ColliderHandle,
        player_collider: ColliderHandle,
    ) -> bool {
        let tracked_contact = if let Some(x) =
            world
                .collider_world()
                .contact_pair(player_collider, trap_collider, true)
        {
            x
        } else {
            return false;
        };

        let contact = tracked_contact.3.deepest_contact().unwrap();
        let mut normal_y = contact.contact.normal.y;

        if tracked_contact.0.handle() != player_collider {
            normal_y *= -1.;
        }

        if normal_y != -1. {
            false
        } else {
            true
        }
    }
}

impl<'a> System<'a> for TrapOpener {
    type SystemData = (
        Entities<'a>,
        Read<'a, LazyUpdate>,
        Write<'a, resources::Map>,
        Read<'a, resources::Tiles>,
        Write<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::Tile>,
        ReadStorage<'a, components::TileActionTrapOpen>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, cmn::components::PlayerProfile>,
        ReadStorage<'a, components::Remanent>,
    );

    fn run(
        &mut self,
        (
            entities,
            builder,
            mut map_res,
            tiles_res,
            mut world_res,
            tile_strg,
            tile_action_trap_strg,
            collider_handle_strg,
            player_profile_strg,
            remanent_strg,
        ): Self::SystemData,
    ) {
        let traps: Vec<(Entity, ColliderHandle, Option<PlayerProfile>)> = (
            &tile_action_trap_strg,
            &entities,
            &collider_handle_strg,
            &tile_strg,
        )
            .join()
            .map(|(_, entity, collider_handle, tile)| {
                (entity, collider_handle.handle(), tile.built_by().clone())
            })
            .collect();

        if traps.len() == 0 {
            return;
        }

        let players: Vec<(PlayerProfile, ColliderHandle)> =
            (&player_profile_strg, &collider_handle_strg)
                .join()
                .map(|(player_profile, player_collider)| {
                    (player_profile.clone(), player_collider.handle())
                })
                .collect();

        let traps_to_remove: Vec<(Entity, ColliderHandle, Option<PlayerProfile>)> = traps
            .iter()
            .cloned()
            .filter(|(_, trap_collider, built_by)| {
                let mut should_remove = false;

                for (profile, player_collider) in players.iter() {
                    if *profile != built_by.clone().unwrap()
                        && Self::is_player_on_top(
                            world_res.world(),
                            *trap_collider,
                            *player_collider,
                        )
                    {
                        should_remove = true;
                        break;
                    }
                }

                should_remove
            })
            .collect();

        for (trap_entity, trap_collider, built_by) in traps_to_remove.iter() {
            let (tile_pos_x, tile_pos_y) = {
                let collider = world_res.world().collider(*trap_collider).unwrap();
                let pos = collider.position().translation.vector;
                (pos.x.floor() as u32, (-pos.y).floor() as u32)
            };

            let mut remanents_to_remove: Vec<Entity> = Vec::new();
            for (entity, remanent) in (&entities, &remanent_strg).join() {
                if remanent.resting_on_handle() == *trap_collider {
                    remanents_to_remove.push(entity);
                }
            }

            for entity in remanents_to_remove.iter() {
                entities
                    .delete(*entity)
                    .expect("Could not remove remanent's entity");
            }

            entities
                .delete(*trap_entity)
                .expect(format!("Couldnt remove trap_entity {:?}", trap_entity).as_str());

            world_res.world_mut().remove_colliders(&[*trap_collider]);

            let new_entity =
                entities::create_tile(builder.create_entity(&entities), built_by.clone());
            map_res.update_entity_at_xy(new_entity, tile_pos_x as i32, tile_pos_y as i32);

            entities::make_tile(
                &builder,
                &tiles_res,
                &TileKey::TrapOpened,
                new_entity,
                tile_pos_x,
                tile_pos_y,
                0.,
            );
        }
    }
}
