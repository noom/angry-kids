use specs::prelude::*;

use crate::{common as cmn, consts::*, game::{
    components,
    entities,
    resources::{self, TileKey},
}};

pub struct SceneCreator;

impl SceneCreator {
    pub fn new() -> Self {
        Self
    }
}

impl<'a> System<'a> for SceneCreator {
    type SystemData = (
        Read<'a, LazyUpdate>,
        Entities<'a>,
        Read<'a, resources::Tiles>,
        Write<'a, resources::PhysicsWorld>,
        Read<'a, resources::Map>,
    );

    fn run(&mut self, (builder, entities, tiles_res, mut world_res, map_res): Self::SystemData) {
        entities::create_peace_wall(builder.create_entity(&entities), &mut world_res);

        entities::create_player(
            builder.create_entity(&entities),
            &mut world_res,
            cmn::components::PlayerProfile::One,
        );

        entities::create_player(
            builder.create_entity(&entities),
            &mut world_res,
            cmn::components::PlayerProfile::Two,
        );

        for y in (MAP_WH.1 - 1)..MAP_WH.1 {
            for x in 0..MAP_WH.0 {
                let tile_entity =
                    if let Some(tile_entity) = map_res.entity_at_xy(x as i32, y as i32) {
                        tile_entity
                    } else {
                        panic!(
                            "Array overflow in SceneCreator system: {}, {} is out of the map!",
                            x, y
                        );
                    };

                entities::make_tile(
                    &builder,
                    &tiles_res,
                    &TileKey::ConstructionWeak,
                    tile_entity,
                    x as u32,
                    y as u32,
                    0.,
                );
            }
        }
    }
}
