use specs::prelude::*;

use nphysics2d::{algebra::Velocity2, object::BodyHandle, world::World as NphysicsWorld};

use ncollide2d::world::CollisionObjectHandle;

use crate::{consts::*, common as cmn, game::{components, resources}};

pub struct Jump;

impl Jump {
    fn is_player_on_other(world: &NphysicsWorld<f32>, player: CollisionObjectHandle) -> bool {
        for x in world
            .collider_world()
            .contacts_with(player, true)
            .expect("player collider does not exists")
        {
            // TODO: Checker au cas ou si ya l'bug
            for contact in x.3.contacts() {
                let reverse = if x.0.handle().uid() < x.1.handle().uid() {
                    1.
                } else {
                    -1.
                };

                if contact.contact.normal.y == -1. * reverse {
                    return true;
                }
            }
        }

        false
    }

    fn can_climbing_jump(
        world: &NphysicsWorld<f32>,
        rigid_body_handle: BodyHandle,
        collider: CollisionObjectHandle,
        sensor: CollisionObjectHandle,
    ) -> bool {
        let position = world
            .collider(collider)
            .expect("Can't find player collider")
            .position()
            .translation
            .vector;

        let rigid_body = world
            .rigid_body(rigid_body_handle)
            .expect("Can't find player rigid_body in physic_world");

        let velocity = rigid_body.velocity().linear;

        if ((position.y % 1. + 1.) % 1. > 0.40) || (velocity.x < 0.1 && velocity.x > -0.1) {
            return false;
        }

        for contact in world
            .collider_world()
            .proximities_with(sensor, true)
            .expect("Can't find player sensor in collider_world")
        {
            let other_collider = if contact.0.handle() == sensor {
                contact.1
            } else if contact.1.handle() == sensor {
                contact.0
            } else {
                continue;
            };

            if other_collider.handle() == collider {
                continue;
            }

            return true;
        }

        false
    }

    fn next_tile(
        world: &NphysicsWorld<f32>,
        map: &resources::Map,
        rigid_body_handle: BodyHandle,
        collider: CollisionObjectHandle,
    ) -> Option<Entity> {
        let position = world
            .collider(collider)
            .expect("Can't find player collider")
            .position()
            .translation
            .vector;

        let rigid_body = world
            .rigid_body(rigid_body_handle)
            .expect("Can't find player rigid_body in physic_world");

        let velocity = rigid_body.velocity().linear;
        let direction = if velocity.x < 0. { -1 } else { 1 };

        let tile_x = position.x.floor() as i32 + direction;
        let tile_y = (-position.y).floor() as i32;

        map.entity_at_xy(tile_x, tile_y)
    }
}

impl<'a> System<'a> for Jump {
    type SystemData = (
        Write<'a, resources::PhysicsWorld>,
        Read<'a, resources::Map>,
        ReadStorage<'a, cmn::components::Controllable>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, components::SensorHandle>,
        ReadStorage<'a, components::RigidBodyHandle>,
        ReadStorage<'a, cmn::components::InputControl>,
    );

    fn run(
        &mut self,
        (
            mut world_res,
            map_res,
            controllable_strg,
            collider_strg,
            sensor_strg,
            rigid_body_strg,
            input_control_strg,
        ): Self::SystemData,
    ) {
        for (_, player_collider, sensor, rigid_body_handle, input_control) in (
            &controllable_strg,
            &collider_strg,
            &sensor_strg,
            &rigid_body_strg,
            &input_control_strg,
        )
            .join()
        {
            let next_tile = Self::next_tile(
                world_res.world(),
                &map_res,
                rigid_body_handle.handle(),
                player_collider.handle(),
            );

            // TODO: Improve climbing jump detection algorithm then re-enable it.
            let climb_jump = match next_tile {
                Some(next_tile) => {
                    Self::can_climbing_jump(
                        world_res.world(),
                        rigid_body_handle.handle(),
                        player_collider.handle(),
                        sensor.handle(),
                    ) && !collider_strg.contains(next_tile)
                }
                None => false,
            };

            if input_control.jump()
                && (Self::is_player_on_other(world_res.world(), player_collider.handle())
                    || (climb_jump && false))
            {
                let rigid_body = world_res
                    .world_mut()
                    .rigid_body_mut(rigid_body_handle.handle())
                    .expect("Can't find player rigid_body in physic_world");

                let velocity = rigid_body.velocity().linear;

                let new_jump_height = JUMP_HEIGHT * if climb_jump { 0.5 } else { 1. };
                let velocity_y = -GRAVITY * JUMP_DURATION * (new_jump_height / JUMP_HEIGHT).sqrt();

                rigid_body.set_velocity(Velocity2::linear(velocity.x, velocity_y));
            }
        }
    }
}
