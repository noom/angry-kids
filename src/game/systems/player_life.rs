use specs::prelude::*;

use nalgebra::{geometry::UnitComplex, Real};

use crate::{
    common as cmn,
    game::{components, resources},
};

pub struct PlayerLife;

impl<'a> System<'a> for PlayerLife {
    type SystemData = (
        Entities<'a>,
        Write<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::Life>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, cmn::components::PlayerProfile>,
        WriteStorage<'a, cmn::components::Controllable>,
        WriteStorage<'a, cmn::components::Animable>,
    );

    fn run(
        &mut self,
        (
            entities,
            mut world_res,
            life_strg,
            collider_handle_strg,
            player_profile_strg,
            mut controllable_strg,
            mut animable_strg,
        ): Self::SystemData,
    ) {
        for (_, entity, life, collider_handle) in (
            &player_profile_strg,
            &entities,
            &life_strg,
            &collider_handle_strg,
        )
            .join()
        {
            if life.life() == 0. {
                let collider = world_res
                    .world_mut()
                    .collider_mut(collider_handle.handle())
                    .expect("Can't find player rigidBody");
                let mut collider_position = collider.position().clone();

                controllable_strg.remove(entity);
                animable_strg.remove(entity);
                collider_position.rotation = UnitComplex::new(f32::frac_pi_2());
                collider.set_position(collider_position);
            }
        }
    }
}
