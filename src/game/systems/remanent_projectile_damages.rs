use specs::prelude::*;

use crate::game::{components, resources};

pub struct RemanentProjectileDamages;

impl<'a> System<'a> for RemanentProjectileDamages {
    type SystemData = (
        Write<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::Projectile>,
        ReadStorage<'a, components::SensorHandle>,
    );

    fn run(&mut self, (mut world_res, projectile_strg, sensor_handle_strg): Self::SystemData) {
        for (projectile, sensor_handle) in (&projectile_strg, &sensor_handle_strg).join() {
            for proximity in world_res
                .world()
                .collider_world()
                .proximities_with(sensor_handle.handle(), true)
                .unwrap()
            {
                // TODO: Tout faire
            }
        }
    }
}
