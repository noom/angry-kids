use specs::prelude::*;

use nphysics2d::algebra::Velocity2;

use crate::{
    common as cmn,
    consts::*,
    game::{components, entities::ColliderTag, resources},
};

pub struct PlayerMovement;

impl<'a> System<'a> for PlayerMovement {
    type SystemData = (
        Write<'a, resources::PhysicsWorld>,
        ReadStorage<'a, cmn::components::Controllable>,
        ReadStorage<'a, components::RigidBodyHandle>,
        ReadStorage<'a, components::SensorHandle>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, cmn::components::InputControl>,
    );

    fn run(
        &mut self,
        (
            mut world_res,
            controllable_strg,
            rigid_body_strg,
            sensor_strg,
            collider_strg,
            input_control_strg,
        ): Self::SystemData,
    ) {
        for (_, rigid_body_handle, sensor, collider, input_control) in (
            &controllable_strg,
            &rigid_body_strg,
            &sensor_strg,
            &collider_strg,
            &input_control_strg,
        )
            .join()
        {
            let mut can_go_left = true;
            let mut can_go_right = true;

            let position_x = world_res
                .world()
                .collider(collider.handle())
                .expect("Can't find player collider")
                .position()
                .translation
                .vector
                .x;

            for contact in world_res
                .world()
                .collider_world()
                .proximities_with(sensor.handle(), true)
                .expect("Can't find player sensor in collider_world")
            {
                let other_collider = if contact.0.handle() == sensor.handle() {
                    contact.1
                } else if contact.1.handle() == sensor.handle() {
                    contact.0
                } else {
                    continue;
                };

                if other_collider.handle() == collider.handle() {
                    continue;
                }

                if let Some(user_data) = other_collider.user_data() {
                    if let Some(extracted_data) = user_data.downcast_ref::<ColliderTag>() {
                        if extracted_data == &ColliderTag::IgnoreCollision {
                            continue;
                        }
                    }
                }

                let other_collider_position = other_collider.position().translation.vector.x;

                if other_collider_position > position_x {
                    can_go_right = false;
                } else {
                    can_go_left = false;
                }
            }

            let rigid_body = world_res
                .world_mut()
                .rigid_body_mut(rigid_body_handle.handle())
                .expect("Can't find player rigid_body in physic_world");

            let velocity = rigid_body.velocity().linear;
            let mut new_x_velocity = 0.;
            let new_y_velocity = velocity.y;

            if input_control.move_left() {
                if can_go_left {
                    new_x_velocity -= PLAYER_SPEED;
                }
            }

            if input_control.move_right() {
                if can_go_right {
                    new_x_velocity += PLAYER_SPEED;
                }
            }

            rigid_body.set_velocity(Velocity2::linear(new_x_velocity, new_y_velocity));
        }
    }
}
