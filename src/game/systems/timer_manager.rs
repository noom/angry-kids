use specs::prelude::*;

use crate::game::{
    components::{self, TimerId},
    resources,
};

pub struct ClockStepper;

impl<'a> System<'a> for ClockStepper {
    type SystemData = WriteStorage<'a, components::Clock>;

    fn run(&mut self, mut clock_strg: Self::SystemData) {
        for (clock,) in (&mut clock_strg,).join() {
            clock.step_all();
        }
    }
}

pub struct ClockEntityRemover;

impl<'a> System<'a> for ClockEntityRemover {
    type SystemData = (
        Entities<'a>,
        Write<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, components::Remanent>,
        ReadStorage<'a, components::Clock>,
    );

    fn run(
        &mut self,
        (entities, mut world_res, collider_handle_strg, remanent_strg, clock_strg): Self::SystemData,
    ) {
        for (entity, collider_handle, clock) in
            (&entities, collider_handle_strg.maybe(), &clock_strg).join()
        {
            if let Some(time_left) = clock.time_left(&TimerId::RemoveEntity) {
                if time_left == 0. {
                    if let Some(collider_handle) = collider_handle {
                        world_res
                            .world_mut()
                            .remove_colliders(&[collider_handle.handle()]);

                        // NOTE: This is a code duplication. See src/construction.rs.
                        // We should find a better and generic way to remove a physical
                        // entity and clean the remanents attached to it.
                        let remanents_to_remove: Vec<Entity> = (&entities, &remanent_strg)
                            .join()
                            .filter_map(|(entity, remanent)| {
                                if remanent.resting_on_handle() == collider_handle.handle() {
                                    Some(entity)
                                } else {
                                    None
                                }
                            })
                            .collect();

                        for entity in remanents_to_remove.iter() {
                            entities
                                .delete(*entity)
                                .expect("Could not remove remanent's entity");
                        }
                    }

                    entities
                        .delete(entity)
                        .expect("Could not delete entity in ClockRemove");
                }
            }
        }
    }
}

pub struct ClockDamagedRemover;

impl<'a> System<'a> for ClockDamagedRemover {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, components::Clock>,
        WriteStorage<'a, components::Damaged>,
    );

    fn run(&mut self, (entities, mut clock_strg, mut damaged_strg): Self::SystemData) {
        let entities_to_clean: Vec<Entity> = (&damaged_strg, &entities, &mut clock_strg)
            .join()
            .filter_map(|(_, entity, clock)| {
                if let Some(time_left) = clock.time_left(&TimerId::RemoveDamaged) {
                    if time_left <= 0. {
                        clock.remove_timer(&TimerId::RemoveDamaged);
                        Some(entity)
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
            .collect();

        for entity in entities_to_clean {
            damaged_strg.remove(entity);
        }
    }
}

// pub struct TimerStepper;

// impl<'a> System<'a> for TimerStepper {
//     type SystemData = WriteStorage<'a, components::Timer>;

//     fn run(&mut self, mut timer_strg: Self::SystemData) {
//         for (timer,) in (&mut timer_strg,).join() {
//             timer.step();
//         }
//     }
// }

// pub struct TimerRemove;

// impl<'a> System<'a> for TimerRemove {
//     type SystemData = (
//         Entities<'a>,
//         WriteStorage<'a, components::Timer>,
//         ReadStorage<'a, components::TimerActionRemove>,
//     );

//     fn run(&mut self, (entities, mut timer_strg, timer_action_remove_strg): Self::SystemData) {
//         for (entity, timer, _) in (&entities, &mut timer_strg, &timer_action_remove_strg).join() {
//             if timer.time_left() == 0. {
//                 entities
//                     .delete(entity)
//                     .expect("Could not delete remanent_timer entity");
//             }
//         }
//     }
// }

// pub struct TimerDamagedRemove;

// impl<'a> System<'a> for TimerDamagedRemove {
//     type SystemData = (
//         Entities<'a>,
//         WriteStorage<'a, components::Timer>,
//         WriteStorage<'a, components::Damaged>,
//     );

//     fn run(&mut self, (entities, mut timer_strg, mut damaged_strg): Self::SystemData) {
//         let entities_to_clean: Vec<Entity> = (&damaged_strg, &entities, &timer_strg)
//             .join()
//             .filter_map(|(_, entity, timer)| {
//                 if timer.time_left() == 0. {
//                     Some(entity)
//                 } else {
//                     None
//                 }
//             })
//             .collect();

//         for entity in entities_to_clean {
//             timer_strg.remove(entity);
//             damaged_strg.remove(entity);
//         }
//     }
// }
