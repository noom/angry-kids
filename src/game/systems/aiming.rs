use specs::prelude::*;

use crate::{
    common as cmn,
    game::{components, components::Direction, resources},
};

pub struct AimingDirection;

impl<'a> System<'a> for AimingDirection {
    type SystemData = (
        ReadStorage<'a, cmn::components::Controllable>,
        ReadStorage<'a, cmn::components::InputControl>,
        WriteStorage<'a, components::FacingDirection>,
    );

    fn run(
        &mut self,
        (controllable_strg, input_control_strg, mut facing_direction_strg): Self::SystemData,
    ) {
        for (_, input_control, facing_direction) in (
            &controllable_strg,
            &input_control_strg,
            &mut facing_direction_strg,
        )
            .join()
        {
            if input_control.aiming_x() < 0. {
                facing_direction.direction = Direction::Left;
            } else {
                facing_direction.direction = Direction::Right;
            }
        }
    }
}

pub struct AimingTile;

impl<'a> System<'a> for AimingTile {
    type SystemData = (
        Read<'a, resources::PhysicsWorld>,
        Read<'a, resources::Map>,
        Entities<'a>,
        ReadStorage<'a, cmn::components::InputControl>,
        ReadStorage<'a, components::ColliderHandle>,
        WriteStorage<'a, components::FacingTile>,
    );

    fn run(
        &mut self,
        (
            world_res,
            map_res,
            entities,
            input_control_strg,
            collider_handle_strg,
            mut facing_tile_strg,
        ): Self::SystemData,
    ) {
        for (entity, input_control, collider_handle) in
            (&entities, &input_control_strg, &collider_handle_strg).join()
        {
            let player_collider = world_res
                .world()
                .collider(collider_handle.handle())
                .unwrap();

            let position = player_collider.position().translation.vector;

            let tile_x = (position.x + input_control.aiming_x()).floor() as i32;
            let tile_y = (-position.y + input_control.aiming_y()).floor() as i32;

            let tile_entity = if let Some(tile_entity) = map_res.entity_at_xy(tile_x, tile_y) {
                tile_entity
            } else {
                if facing_tile_strg.contains(entity) {
                    facing_tile_strg.remove(entity);
                }
                continue;
            };

            // Does the player contain a FacingTile?
            //  Yes. Is the FacingTile the same as the newly detected one?
            //   Yes. Next iteration.
            //  No. Update it.
            // No. Make a new one and insert it.
            if let Some(facing_tile) = facing_tile_strg.get_mut(entity) {
                let facing_tile_entity = map_res
                    .entity_at_xy(facing_tile.x as i32, facing_tile.y as i32)
                    .unwrap();

                if facing_tile_entity == tile_entity {
                    continue;
                } else {
                    facing_tile.x = tile_x as u32;
                    facing_tile.y = tile_y as u32;
                }
            } else {
                let facing_tile = components::FacingTile::new(tile_x as u32, tile_y as u32);

                facing_tile_strg
                    .insert(entity, facing_tile)
                    .expect("Couldn't insert new FacingTile in FacingTileDetector system");
            }
        }
    }
}
