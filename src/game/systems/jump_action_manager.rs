use specs::prelude::*;

use nphysics2d::world::World as NphysicsWorld;

use nalgebra::base::Vector2;

use crate::{
    common as cmn,
    consts::*,
    game::{components, resources},
};

// TODO: Refactor to BounceActionManager
pub struct JumpActionManager;

impl JumpActionManager {
    pub fn jump(
        world: &mut NphysicsWorld<f32>,
        tile_collider: &components::ColliderHandle,
        player_collider: &components::ColliderHandle,
        rigid_body_handle: &components::RigidBodyHandle,
        // TODO: Also check on Controllable for extended bounce
        input_control: Option<&cmn::components::InputControl>,
    ) {
        let tracked_contact = if let Some(x) = world.collider_world().contact_pair(
            player_collider.handle(),
            tile_collider.handle(),
            true,
        ) {
            x
        } else {
            return;
        };

        let contact = tracked_contact.3.deepest_contact().unwrap();
        let mut normal_y = contact.contact.normal.y;

        if tracked_contact.0.handle() != player_collider.handle() {
            normal_y *= -1.;
        }

        if normal_y != -1. {
            return;
        }

        let new_jump_height = if let Some(input_control) = input_control {
            if input_control.jump() {
                2. * JUMP_HEIGHT
            } else {
                JUMP_HEIGHT
            }
        } else {
            JUMP_HEIGHT
        };

        let velocity_y = -GRAVITY * JUMP_DURATION * (new_jump_height / JUMP_HEIGHT).sqrt();

        let rigid_body = world.rigid_body_mut(rigid_body_handle.handle()).unwrap();
        let mut velocity = rigid_body.velocity().clone();
        velocity.linear = Vector2::new(velocity.linear.x, velocity_y);

        rigid_body.set_velocity(velocity);
    }
}

impl<'a> System<'a> for JumpActionManager {
    type SystemData = (
        Write<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::TileActionJump>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, components::RigidBodyHandle>,
        ReadStorage<'a, cmn::components::InputControl>,
    );

    fn run(
        &mut self,
        (
            mut world_res,
            tile_action_jump_strg,
            collider_handle_strg,
            rigid_body_handle_strg,
            input_control_strg,
        ): Self::SystemData,
    ) {
        let mut tile_events = Vec::new();

        for (_, collider_handle) in (&tile_action_jump_strg, &collider_handle_strg).join() {
            tile_events.push(collider_handle);
        }

        if tile_events.len() == 0 {
            return;
        }

        for (input_control, collider_handle, rigid_body_handle) in (
            input_control_strg.maybe(),
            &collider_handle_strg,
            &rigid_body_handle_strg,
        )
            .join()
        {
            for tile in tile_events.iter() {
                Self::jump(
                    world_res.world_mut(),
                    tile,
                    collider_handle,
                    rigid_body_handle,
                    input_control,
                )
            }
        }
    }
}
