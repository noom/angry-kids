use specs::prelude::*;

use nphysics2d::object::BodyPartHandle;

use crate::{
    common as cmn,
    consts::*,
    game::{
        components, entities,
        resources::{self, TileKey},
    },
};

pub struct ConstructionAppearing;

impl<'a> System<'a> for ConstructionAppearing {
    type SystemData = (
        Entities<'a>,
        Write<'a, resources::PhysicsWorld>,
        WriteStorage<'a, components::Construction>,
        WriteStorage<'a, components::Clock>,
        WriteStorage<'a, components::ColliderHandle>,
    );

    fn run(
        &mut self,
        (entities, mut world_res, mut construction_strg, mut clock_strg, mut collider_handle_strg): Self::SystemData,
    ) {
        let mut remove_constructions = Vec::new();

        for (entity, construction, clock) in
            (&entities, &mut construction_strg, &mut clock_strg).join()
        {
            if clock.time_left(&components::TimerId::ConstructingTile) == Some(0.) {
                let collider_desc = construction.collider_desc();
                let collider_handle = collider_desc
                    .build_with_parent(BodyPartHandle::ground(), world_res.world_mut())
                    .unwrap()
                    .handle();

                remove_constructions.push(entity);
                clock.remove_timer(&components::TimerId::ConstructingTile);

                collider_handle_strg
                    .insert(entity, components::ColliderHandle::new(collider_handle))
                    .expect("Couldnt insert ColliderHandle");
            }
        }

        for construction in remove_constructions.iter() {
            construction_strg.remove(*construction);
        }
    }
}

pub struct ConstructionDisappearing;

impl<'a> System<'a> for ConstructionDisappearing {
    type SystemData = (
        Read<'a, LazyUpdate>,
        Entities<'a>,
        Write<'a, resources::Map>,
        Write<'a, resources::PhysicsWorld>,
        WriteStorage<'a, components::Destruction>,
        WriteStorage<'a, components::Clock>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, components::Remanent>,
    );

    fn run(
        &mut self,
        (
            builder,
            entities,
            mut map_res,
            mut world_res,
            destruction_strg,
            mut clock_strg,
            collider_handle_strg,
            remanent_strg,
        ): Self::SystemData,
    ) {
        for (destruction, clock) in (&destruction_strg, &mut clock_strg).join() {
            if let Some(timer) = clock.time_left(&components::TimerId::DestructingTile) {
                if timer > 0. {
                    continue;
                }
            }

            let tile_pos = destruction.tile_pos();

            let tile_entity = map_res
                .entity_at_xy(tile_pos.0 as i32, tile_pos.1 as i32)
                .unwrap();

            if !collider_handle_strg.contains(tile_entity) {
                continue;
            }

            let collider_handle = collider_handle_strg.get(tile_entity).unwrap();

            let remanents_to_remove: Vec<Entity> = (&entities, &remanent_strg)
                .join()
                .filter_map(|(entity, remanent)| {
                    if remanent.resting_on_handle() == collider_handle.handle() {
                        Some(entity)
                    } else {
                        None
                    }
                })
                .collect();

            for entity in remanents_to_remove.iter() {
                entities
                    .delete(*entity)
                    .expect("Could not remove remanent's entity");
            }

            entities
                .delete(tile_entity)
                .expect("Couldn't delete Tile entity from FacingTile");

            world_res
                .world_mut()
                .remove_colliders(&[collider_handle_strg.get(tile_entity).unwrap().handle()]);

            let tile_entity = entities::create_tile(builder.create_entity(&entities), None);

            map_res.update_entity_at_xy(tile_entity, tile_pos.0 as i32, tile_pos.1 as i32);
        }
    }
}

pub struct Construction;

impl<'a> System<'a> for Construction {
    type SystemData = (
        Read<'a, LazyUpdate>,
        Read<'a, resources::Map>,
        Read<'a, resources::Tiles>,
        WriteStorage<'a, components::Tile>,
        ReadStorage<'a, cmn::components::PlayerProfile>,
        ReadStorage<'a, cmn::components::Controllable>,
        WriteStorage<'a, cmn::components::InputControl>,
        ReadStorage<'a, components::ColliderHandle>,
        WriteStorage<'a, components::FacingTile>,
        ReadStorage<'a, components::Selector<TileKey>>,
        ReadStorage<'a, components::Construction>,
        WriteStorage<'a, components::Destruction>,
        WriteStorage<'a, components::Clock>,
    );

    fn run(
        &mut self,
        (
            builder,
            map_res,
            tiles_res,
            mut tile_strg,
            player_profile_strg,
            controllable_strg,
            mut input_control_strg,
            collider_handle_strg,
            mut facing_tile_strg,
            tile_selector_strg,
            construction_strg,
            mut destruction_strg,
            mut clock_strg,
        ): Self::SystemData,
    ) {
        for (_, player_profile, input_control, facing_tile, tile_selector) in (
            &controllable_strg,
            &player_profile_strg,
            &mut input_control_strg,
            &mut facing_tile_strg,
            &tile_selector_strg,
        )
            .join()
        {
            if input_control.build {
                input_control.build = false;

                let tile_entity = map_res
                    .entity_at_xy(facing_tile.x as i32, facing_tile.y as i32)
                    .unwrap();

                if construction_strg.contains(tile_entity)
                    || collider_handle_strg.contains(tile_entity)
                {
                    continue;
                }

                tile_strg
                    .get_mut(tile_entity)
                    .unwrap()
                    .set_built_by(player_profile.clone());

                entities::make_tile(
                    &builder,
                    &tiles_res,
                    tile_selector.selected(),
                    tile_entity,
                    facing_tile.x,
                    facing_tile.y,
                    CONSTRUCTION_TIME,
                );
            } else if input_control.destroy {
                input_control.destroy = false;

                let tile_entity = map_res
                    .entity_at_xy(facing_tile.x as i32, facing_tile.y as i32)
                    .unwrap();

                if destruction_strg.contains(tile_entity)
                    || !collider_handle_strg.contains(tile_entity)
                {
                    continue;
                }

                destruction_strg
                    .insert(
                        tile_entity,
                        components::Destruction::new(
                            collider_handle_strg.get(tile_entity).unwrap().handle(),
                            (facing_tile.x, facing_tile.y),
                        ),
                    )
                    .expect("Couldnt add destruction component to entity");

                let mut clock = components::Clock::default();

                clock.add_timer(components::TimerId::DestructingTile, CONSTRUCTION_TIME);

                clock_strg
                    .insert(tile_entity, clock)
                    .expect("Couldn't add clock component to entity");
            }
        }
    }
}
