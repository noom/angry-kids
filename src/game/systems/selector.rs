use specs::prelude::*;

use crate::{
    common as cmn,
    consts::*,
    game::{
        components::{self, DisplaySelectorState, TimerId},
        resources::{ProjectileKey, TileKey},
    },
};

pub struct DisplaySelectorManager;

impl DisplaySelectorManager {
    fn display_selector_manager<T: Send + Sync>(
        appear: bool,
        selector: &mut components::Selector<T>,
        clock: &mut components::Clock,
        timer_id: &TimerId,
    ) {
        if appear {
            match selector.state() {
                DisplaySelectorState::Invisible => {
                    selector.set_state(DisplaySelectorState::FadingIn);
                    clock.set_duration(timer_id, SELECTOR_FADING_DURATION);
                    clock.reset(timer_id);
                }
                DisplaySelectorState::FadingOut => {
                    selector.set_state(DisplaySelectorState::FadingIn)
                }
                DisplaySelectorState::Visible => clock.reset(timer_id),
                _ => (),
            }
        } else if clock
            .time_left(timer_id)
            .expect("Display selector doesn't have timer")
            == 0.
        {
            let new_state = match selector.state() {
                DisplaySelectorState::FadingIn => DisplaySelectorState::Visible,
                DisplaySelectorState::Visible => DisplaySelectorState::FadingOut,
                DisplaySelectorState::FadingOut => DisplaySelectorState::Invisible,
                DisplaySelectorState::Invisible => return,
            };

            if new_state == DisplaySelectorState::Visible {
                clock.set_duration(timer_id, SELECTOR_DISPLAYING_DURATION);
                clock.reset(timer_id);
            } else if new_state == DisplaySelectorState::FadingOut {
                clock.set_duration(timer_id, SELECTOR_FADING_DURATION);
                clock.reset(timer_id);
            }

            selector.set_state(new_state);
        }
    }
}

impl<'a> System<'a> for DisplaySelectorManager {
    type SystemData = (
        WriteStorage<'a, cmn::components::InputControl>,
        WriteStorage<'a, components::Selector<TileKey>>,
        WriteStorage<'a, components::Selector<ProjectileKey>>,
        WriteStorage<'a, components::Clock>,
    );

    fn run(
        &mut self,
        (
            mut input_control_strg,
            mut tile_selector_strg,
            mut projectile_selector_strg,
            mut clock_strg,
        ): Self::SystemData,
    ) {
        for (input_control, tile_selector, projectile_selector, clock) in (
            &mut input_control_strg,
            &mut tile_selector_strg,
            &mut projectile_selector_strg,
            &mut clock_strg,
        )
            .join()
        {
            if input_control.prev_tile || input_control.next_tile {
                projectile_selector.set_state(DisplaySelectorState::Invisible);
                clock.reset(&TimerId::DisplayTileSelector);
            } else if input_control.prev_projectile || input_control.next_projectile {
                tile_selector.set_state(DisplaySelectorState::Invisible);
                clock.reset(&TimerId::DisplayProjectileSelector);
            }

            Self::display_selector_manager(
                input_control.prev_tile || input_control.next_tile,
                tile_selector,
                clock,
                &TimerId::DisplayTileSelector,
            );

            Self::display_selector_manager(
                input_control.prev_projectile || input_control.next_projectile,
                projectile_selector,
                clock,
                &TimerId::DisplayProjectileSelector,
            );
        }
    }
}

pub struct SelectorChanger;

impl<'a> System<'a> for SelectorChanger {
    type SystemData = (
        WriteStorage<'a, cmn::components::InputControl>,
        WriteStorage<'a, components::Selector<TileKey>>,
        WriteStorage<'a, components::Selector<ProjectileKey>>,
    );

    fn run(
        &mut self,
        (
            mut input_control_strg,
            mut tile_selector_strg,
            mut projectile_selector_strg,
        ): Self::SystemData,
    ) {
        for (input_control, tile_selector, projectile_selector) in (
            &mut input_control_strg,
            &mut tile_selector_strg,
            &mut projectile_selector_strg,
        )
            .join()
        {
            if input_control.prev_tile {
                tile_selector.selection_prev();
                input_control.prev_tile = false;
            }

            if input_control.next_tile {
                tile_selector.selection_next();
                input_control.next_tile = false;
            }

            if input_control.prev_projectile {
                projectile_selector.selection_prev();
                input_control.prev_projectile = false;
            }

            if input_control.next_projectile {
                projectile_selector.selection_next();
                input_control.next_projectile = false;
            }
        }
    }
}
