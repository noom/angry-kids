use specs::prelude::*;

use nphysics2d::{algebra::Velocity2, object::ColliderHandle, world::World as NphysicsWorld};

use crate::{
    common as cmn,
    consts::*,
    game::{components, entities::ColliderTag, resources},
};

pub struct LadderClimbing;

impl LadderClimbing {
    fn is_player_on_ladder(
        world: &NphysicsWorld<f32>,
        map: &Read<'_, resources::Map>,
        tile_action_climb_strg: &ReadStorage<'_, components::TileActionClimb>,
        player: ColliderHandle,
    ) -> bool {
        for proximity in world
            .collider_world()
            .proximities_with(player, true)
            .expect("player collider does not exists")
        {
            if let Some(user_data) = proximity.1.user_data() {
                if let Some(extracted_data) = user_data.downcast_ref::<ColliderTag>() {
                    if extracted_data == &ColliderTag::IgnoreCollision {
                        let pos = proximity.1.position().translation.vector;
                        let tile_entity = map
                            .entity_at_xy(pos.x.floor() as i32, (-pos.y).floor() as i32)
                            .expect("no tile entity there");

                        if tile_action_climb_strg.contains(tile_entity) {
                            return true;
                        }
                    }
                }

                return false;
            } else {
                return false;
            }
        }

        false
    }

    pub fn is_player_colliding(
        world: &NphysicsWorld<f32>,
        player_collider: ColliderHandle,
    ) -> bool {
        if let Some(contacts) = world.collider_world().contacts_with(player_collider, true) {
            contacts.count() > 0
        } else {
            false
        }
    }
}

impl<'a> System<'a> for LadderClimbing {
    type SystemData = (
        Read<'a, resources::Map>,
        Write<'a, resources::PhysicsWorld>,
        ReadStorage<'a, components::TileActionClimb>,
        ReadStorage<'a, cmn::components::Controllable>,
        ReadStorage<'a, components::ColliderHandle>,
        ReadStorage<'a, components::RigidBodyHandle>,
        ReadStorage<'a, cmn::components::InputControl>,
    );

    fn run(
        &mut self,
        (
            map_res,
            mut world_res,
            tile_action_climb_strg,
            controllable_strg,
            collider_handle_strg,
            rigid_body_handle_strg,
            input_control_strg,
        ): Self::SystemData,
    ) {
        for (_, collider_handle, rigid_body_handle, input_control) in (
            &controllable_strg,
            &collider_handle_strg,
            &rigid_body_handle_strg,
            &input_control_strg,
        )
            .join()
        {
            if !Self::is_player_colliding(world_res.world(), collider_handle.handle())
                && Self::is_player_on_ladder(
                    world_res.world(),
                    &map_res,
                    &tile_action_climb_strg,
                    collider_handle.handle(),
                )
            {
                let rigid_body = world_res
                    .world_mut()
                    .rigid_body_mut(rigid_body_handle.handle())
                    .unwrap();
                rigid_body.disable_all_translations();

                let velocity = rigid_body.velocity().linear;

                let modifier;
                if input_control.jump() {
                    modifier = -1.;
                } else if input_control.move_down() {
                    modifier = 1.;
                } else {
                    continue;
                }

                let new_jump_height = JUMP_HEIGHT * (CLIMBING_SPEED / JUMP_HEIGHT);
                let velocity_y =
                    modifier * GRAVITY * JUMP_DURATION * (new_jump_height / JUMP_HEIGHT).sqrt();

                rigid_body.set_velocity(Velocity2::linear(velocity.x, velocity_y));
            } else {
                let rigid_body = world_res
                    .world_mut()
                    .rigid_body_mut(rigid_body_handle.handle())
                    .unwrap();

                rigid_body.enable_all_translations();
            }
        }
    }
}
