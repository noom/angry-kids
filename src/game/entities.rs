use specs::prelude::*;

use nphysics2d::{
    algebra::Velocity2,
    object::{BodyPartHandle, ColliderDesc, RigidBodyDesc},
};

use ncollide2d::shape::{Cuboid, ShapeHandle};

use nalgebra::{Isometry2, Vector2};

use crate::{
    assets::{AssetKey, Assets, Selectable},
    common::{self as cmn, PlayerProfile},
    consts::*,
    game::{
        components::{self, TimerId},
        resources::{self, ProjectileKey, TileKey},
    },
};

#[derive(Clone, PartialEq)]
pub enum ColliderTag {
    ForceCollision,
    IgnoreCollision,
}

pub fn create_peace_wall(
    builder: impl Builder,
    physics_world: &mut Write<'_, resources::PhysicsWorld>,
) {
    // let width = (SCREEN_WH.0 / 3) as f32;
    let width = (MAP_WH.0 / 3) as f32;
    let height = MAP_WH.1 as f32;

    let shape_handle = ShapeHandle::new(Cuboid::new(Vector2::new(
        width / 2. - COLLIDER_MARGIN,
        height / 2. - COLLIDER_MARGIN,
    )));

    let position = Isometry2::new(Vector2::new(MAP_WH.0 as f32 / 2., height / -2.), 0.);

    let collider_desc = ColliderDesc::new(shape_handle)
        .position(position)
        .margin(COLLIDER_MARGIN);

    let collider_handle = collider_desc
        .build_with_parent(BodyPartHandle::ground(), physics_world.world_mut())
        .unwrap()
        .handle();

    let collider_handle = components::ColliderHandle::new(collider_handle);
    let mut clock = components::Clock::default();
    clock.add_timer(TimerId::RemoveEntity, PEACE_WALL_DURATION);
    let peace_wall = components::PeaceWall::default();

    builder.with(collider_handle).with(clock).with(peace_wall);
}

pub fn create_projectile(
    builder: &Read<'_, LazyUpdate>,
    entities: &Entities<'_>,
    projectiles_res: &Read<'_, resources::Projectiles>,
    physics_world: &mut Write<'_, resources::PhysicsWorld>,
    projectile_type: ProjectileKey,
    player_pos: Vector2<f32>,
    aiming: Vector2<f32>,
) {
    let asset_key = projectiles_res.asset_key_for(&projectile_type);

    let velocity = resources::Projectiles::initial_velocity_for(&projectile_type, aiming);

    let (width, height) = projectiles_res.size_for(&projectile_type);

    let shape_handle = ShapeHandle::new(Cuboid::new(Vector2::new(
        width / 2. - COLLIDER_MARGIN,
        height / 2. - COLLIDER_MARGIN,
    )));

    let position = Isometry2::new(
        Vector2::new(player_pos.x + aiming.x, player_pos.y + aiming.y),
        velocity.linear.x.atan2(velocity.linear.y),
    );

    let collider_desc = ColliderDesc::new(shape_handle)
        .user_data(ColliderTag::ForceCollision)
        .density(1.)
        .margin(COLLIDER_MARGIN);

    let rigid_body = RigidBodyDesc::new()
        .position(position)
        .velocity(velocity)
        .build(physics_world.world_mut());

    let rigid_body_parent_handle = rigid_body.part_handle();
    let rigid_body_handle = rigid_body.handle();

    let collider_handle = collider_desc
        .build_with_parent(rigid_body_parent_handle, physics_world.world_mut())
        .unwrap()
        .handle();

    let entity = builder
        .create_entity(entities)
        .with(components::ColliderHandle::new(collider_handle))
        .with(components::RigidBodyHandle::new(rigid_body_handle))
        .with(components::Projectile::new(projectile_type.clone()))
        .with(cmn::components::Image::new(asset_key.clone()))
        .with(components::Clock::default())
        .build();

    resources::Projectiles::add_tags(builder, entity, &projectile_type);
}

pub fn create_player(
    builder: impl Builder,
    physics_world: &mut Write<'_, resources::PhysicsWorld>,
    player_profile: PlayerProfile,
) {
    let shape_handle = ShapeHandle::new(Cuboid::new(Vector2::new(
        PLAYER_WH.0 / 2. - COLLIDER_MARGIN,
        PLAYER_WH.1 / 2. - COLLIDER_MARGIN,
    )));

    let position_x = match player_profile {
        PlayerProfile::One => 3.,
        PlayerProfile::Two => NPHYSICS_WORLD_WH.0 - 3.,
    };
    let position = Isometry2::new(Vector2::new(position_x, -10.), 0.);

    let collider_desc = ColliderDesc::new(shape_handle)
        .position(position)
        .density(1.)
        .margin(COLLIDER_MARGIN);

    let rigid_body = RigidBodyDesc::new().build(physics_world.world_mut());
    rigid_body.disable_all_rotations();

    let rigid_body_parent_handle = rigid_body.part_handle();
    let rigid_body_handle = rigid_body.handle();

    let collider_handle = collider_desc
        .build_with_parent(rigid_body_parent_handle, physics_world.world_mut())
        .unwrap()
        .handle();

    let shape_sensor_handle = ShapeHandle::new(Cuboid::new(Vector2::new(
        PLAYER_WH.0 / 2. + 0.1,
        PLAYER_WH.1 / 2. - COLLIDER_MARGIN - 0.025,
    )));

    let sensor_desc = ColliderDesc::new(shape_sensor_handle)
        .position(position)
        .density(1.)
        .margin(COLLIDER_MARGIN)
        .sensor(true);

    let sensor_handle = sensor_desc
        .build_with_parent(rigid_body_parent_handle, physics_world.world_mut())
        .unwrap()
        .handle();
    let mut clock = components::Clock::default();
    clock.add_timer(TimerId::DisplayProjectileSelector, 0.);
    clock.add_timer(TimerId::DisplayTileSelector, 0.);

    builder
        .with(cmn::components::Controllable)
        .with(player_profile)
        .with(components::Selector::<TileKey>::new(vec![
            TileKey::Spikes,
            TileKey::Ladder,
            TileKey::ConstructionWeak,
            TileKey::ConstructionStrong,
            TileKey::Trampoline,
            TileKey::TrapClosed,
        ]))
        .with(components::Selector::<ProjectileKey>::new(vec![
            ProjectileKey::Syringe,
            ProjectileKey::BrokenBottle,
        ]))
        .with(cmn::components::InputControl::default())
        .with(components::ColliderHandle::new(collider_handle))
        .with(components::RigidBodyHandle::new(rigid_body_handle))
        .with(cmn::components::Image::new(AssetKey::ChickenAnimated).with_tile_number(0))
        .with(components::SensorHandle::new(sensor_handle))
        .with(cmn::components::Animable::new(0, 6))
        .with(components::FacingDirection::default())
        .with(components::Life::new(LIFE_AMOUNT))
        .with(clock)
        .build();
}

pub fn create_tile(builder: impl Builder, built_by: Option<PlayerProfile>) -> Entity {
    builder
        .with(components::Tile::new(TileKey::Void, built_by))
        .build()
}

pub fn make_tile(
    builder: &Read<'_, LazyUpdate>,
    tiles_res: &Read<'_, resources::Tiles>,
    key: &TileKey,
    entity: Entity,
    x: u32,
    y: u32,
    construction_time: f32,
) {
    let tile_size = tiles_res.size_for(key);

    let shape_handle = ShapeHandle::new(Cuboid::new(Vector2::new(
        (tile_size.0 / 2.) - COLLIDER_MARGIN,
        (tile_size.1 / 2.) - COLLIDER_MARGIN,
    )));

    let position = Isometry2::new(
        Vector2::new(
            x as f32 + TILE_SIZE - tile_size.0 / 2.,
            -(y as f32 + TILE_SIZE - tile_size.1 / 2.),
        ),
        0.,
    );

    let mut collider_desc = ColliderDesc::new(shape_handle)
        .position(position)
        .margin(COLLIDER_MARGIN);

    collider_desc = resources::Tiles::setup_collider(collider_desc, key);

    let construction = components::Construction::new(collider_desc);
    let mut clock = components::Clock::default();
    clock.add_timer(components::TimerId::ConstructingTile, construction_time);
    let image = cmn::components::Image::new(tiles_res.asset_key_for(key).clone());

    builder.insert(entity, construction);
    builder.insert(entity, clock);
    builder.insert(entity, image);

    resources::Tiles::add_tags(builder, entity, key);
}
