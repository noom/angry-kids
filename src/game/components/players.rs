use specs::prelude::*;
use specs_derive::Component;

use nalgebra::Real;

use crate::consts::*;

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct Damaged;

#[derive(Clone, Copy, PartialEq)]
pub enum Direction {
    Left = -1,
    Right = 1,
}

#[derive(Component)]
pub struct FacingDirection {
    pub direction: Direction,
}

impl Default for FacingDirection {
    fn default() -> Self {
        Self {
            direction: Direction::Left,
        }
    }
}

#[derive(Component, Debug)]
pub struct FacingTile {
    pub x: u32,
    pub y: u32,
}

impl FacingTile {
    pub fn new(x: u32, y: u32) -> Self {
        Self { x, y }
    }
}

#[derive(Component, Debug)]
pub struct Life(f32);

impl Life {
    pub fn new(life: f32) -> Self {
        Self(life)
    }

    pub fn life(&self) -> f32 {
        self.0
    }

    pub fn set_life(&mut self, new_life: f32) {
        self.0 = new_life;
    }

    pub fn change_by(&mut self, amount: f32) {
        if amount < 0. && (-amount as f32) < self.0 {
            self.0 -= -amount as f32;
        } else if amount >= 0. {
            self.0 += amount as f32;
        } else {
            self.0 = 0.;
        }
    }
}

