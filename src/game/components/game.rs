use specs::prelude::*;
use specs_derive::Component;

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct PeaceWall;
