use std::collections::HashMap;

use specs::prelude::*;
use specs_derive::Component;

use crate::consts::*;

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct TimerActionRemove;

pub struct Timer {
    duration: f32,
    progress: f32,
}

impl Timer {
    pub fn new(duration: f32) -> Self {
        Self {
            duration,
            progress: 0.,
        }
    }

    pub fn step(&mut self) -> bool {
        if self.progress < self.duration {
            self.progress += DT;

            false
        } else {
            true
        }
    }

    pub fn reset(&mut self) {
        self.progress = 0.;
    }

    pub fn time_left(&self) -> f32 {
        let time_left = self.duration - self.progress;

        if time_left < 0. {
            0.
        } else {
            time_left
        }
    }

    pub fn duration(&self) -> f32 {
        self.duration
    }

    pub fn progress(&self) -> f32 {
        self.progress
    }

    pub fn progress_perc(&self) -> f32 {
        self.progress / self.duration
    }

    pub fn set_duration(&mut self, duration: f32) {
        self.duration = duration;
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum TimerId {
    RemoveDamaged,
    RemoveEntity,
    ConstructingTile,
    DestructingTile,
    DisplayTileSelector,
    DisplayProjectileSelector,
}

#[derive(Component)]
pub struct Clock {
    timers: HashMap<TimerId, Timer>,
}

impl Default for Clock {
    fn default() -> Self {
        Self {
            timers: HashMap::new(),
        }
    }
}

impl Clock {
    pub fn add_timer(&mut self, timer_id: TimerId, duration: f32) {
        self.timers.insert(timer_id, Timer::new(duration));
    }

    pub fn remove_timer(&mut self, timer_id: &TimerId) {
        self.timers.remove(timer_id);
    }

    pub fn timer(&self, timer_id: &TimerId) -> Option<&Timer> {
        self.timers.get(timer_id)
    }

    pub fn timer_mut(&mut self, timer_id: &TimerId) -> Option<&mut Timer> {
        self.timers.get_mut(timer_id)
    }

    pub fn step(&mut self, timer_id: &TimerId) -> Option<bool> {
        if let Some(timer) = self.timers.get_mut(timer_id) {
            Some(timer.step())
        } else {
            None
        }
    }

    pub fn step_all(&mut self) {
        for (_, timer) in self.timers.iter_mut() {
            timer.step();
        }
    }

    pub fn reset(&mut self, timer_id: &TimerId) {
        if let Some(timer) = self.timers.get_mut(timer_id) {
            timer.reset();
        }
    }

    pub fn time_left(&self, timer_id: &TimerId) -> Option<f32> {
        if let Some(timer) = self.timers.get(timer_id) {
            Some(timer.time_left())
        } else {
            None
        }
    }

    pub fn duration(&self, timer_id: &TimerId) -> Option<f32> {
        if let Some(timer) = self.timers.get(timer_id) {
            Some(timer.duration())
        } else {
            None
        }
    }

    pub fn progress(&self, timer_id: &TimerId) -> Option<f32> {
        if let Some(timer) = self.timers.get(timer_id) {
            Some(timer.progress())
        } else {
            None
        }
    }

    pub fn progress_perc(&self, timer_id: &TimerId) -> Option<f32> {
        if let Some(timer) = self.timers.get(timer_id) {
            Some(timer.progress_perc())
        } else {
            None
        }
    }

    pub fn set_duration(&mut self, timer_id: &TimerId, duration: f32) {
        if let Some(timer) = self.timers.get_mut(timer_id) {
            timer.set_duration(duration);
        }
    }
}
