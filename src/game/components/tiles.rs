use specs::prelude::*;
use specs_derive::Component;

use nphysics2d::object;

use crate::{common::PlayerProfile, game::{resources::TileKey}};

#[derive(Component)]
pub struct Tile {
    key: TileKey,
    built_by: Option<PlayerProfile>,
}

impl Tile {
    pub fn new(key: TileKey, built_by: Option<PlayerProfile>) -> Self {
        Self { key, built_by }
    }

    pub fn key(&self) -> &TileKey {
        &self.key
    }

    pub fn built_by(&self) -> &Option<PlayerProfile> {
        &self.built_by
    }

    pub fn set_built_by(&mut self, built_by: PlayerProfile) {
        self.built_by = Some(built_by);
    }
}

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct TileActionJump;

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct TileActionTrapOpen;

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct TileActionTrapClose;

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct TileActionClimb;

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct TileActionDealDamage;

#[derive(Component)]
pub struct Construction {
    collider_desc: object::ColliderDesc<f32>,
}

impl Construction {
    pub fn new(collider_desc: object::ColliderDesc<f32>) -> Self {
        Self { collider_desc }
    }

    pub fn collider_desc(&self) -> &object::ColliderDesc<f32> {
        &self.collider_desc
    }
}

#[derive(Component)]
pub struct Destruction {
    collider_handle: object::ColliderHandle,
    tile_pos: (u32, u32),
}

impl Destruction {
    pub fn new(collider_handle: object::ColliderHandle, tile_pos: (u32, u32)) -> Self {
        Self {
            collider_handle,
            tile_pos,
        }
    }

    pub fn tile_pos(&self) -> &(u32, u32) {
        &self.tile_pos
    }

    pub fn collider_handle(&self) -> &object::ColliderHandle {
        &self.collider_handle
    }
}
