use specs::prelude::*;
use specs_derive::Component;

#[derive(PartialEq, Debug)]
pub enum DisplaySelectorState {
    FadingIn,
    FadingOut,
    Visible,
    Invisible,
}

#[derive(Component)]
pub struct Selector<T: 'static + Send + Sync> {
    objects: Vec<T>,
    selection: usize,
    display: DisplaySelectorState,
}

impl<T: Send + Sync> Selector<T> {
    pub fn new(objects: Vec<T>) -> Self {
        Self {
            objects,
            selection: 0,
            display: DisplaySelectorState::Invisible,
        }
    }

    pub fn objects(&self) -> &Vec<T> {
        &self.objects
    }

    pub fn selection_index(&self) -> usize {
        self.selection
    }

    pub fn selected(&self) -> &T {
        &self.objects[self.selection]
    }

    pub fn selection_prev(&mut self) {
        if self.selection == 0 {
            self.selection = self.objects.len() - 1;
        } else {
            self.selection -= 1;
        }
    }

    pub fn selection_next(&mut self) {
        if self.selection == self.objects.len() - 1 {
            self.selection = 0;
        } else {
            self.selection += 1;
        }
    }

    pub fn state(&self) -> &DisplaySelectorState {
        &self.display
    }

    pub fn set_state(&mut self, state: DisplaySelectorState) {
        self.display = state;
    }
}
