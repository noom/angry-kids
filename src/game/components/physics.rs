use specs::prelude::*;
use specs_derive::Component;

use nphysics2d::object;

#[derive(Component)]
pub struct ColliderHandle(object::ColliderHandle);

impl ColliderHandle {
    pub fn new(handle: object::ColliderHandle) -> Self {
        Self(handle)
    }

    pub fn handle(&self) -> object::ColliderHandle {
        self.0
    }
}

#[derive(Component)]
pub struct RigidBodyHandle(object::BodyHandle);

impl RigidBodyHandle {
    pub fn new(handle: object::BodyHandle) -> Self {
        Self(handle)
    }

    pub fn handle(&self) -> object::BodyHandle {
        self.0
    }
}

#[derive(Component)]
pub struct SensorHandle(object::ColliderHandle);

impl SensorHandle {
    pub fn new(handle: object::ColliderHandle) -> Self {
        Self(handle)
    }

    pub fn handle(&self) -> object::ColliderHandle {
        self.0
    }
}
