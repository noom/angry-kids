use specs::prelude::*;
use specs_derive::Component;

use nphysics2d::object;

use nalgebra::Isometry2;

use crate::game::resources::ProjectileKey;

#[derive(Component)]
pub struct Projectile {
    key: ProjectileKey,
}

impl Projectile {
    pub fn new(key: ProjectileKey) -> Self {
        Self { key }
    }

    pub fn projectile_key(&self) -> &ProjectileKey {
        &self.key
    }
}

impl Default for Projectile {
    fn default() -> Self {
        Self {
            key: ProjectileKey::BrokenBottle,
        }
    }
}

#[derive(Component)]
pub struct ProjectileActionDamage(pub i32);

#[derive(Component, Default)]
#[storage(NullStorage)]
pub struct ProjectileRotation;

#[derive(Component)]
pub struct Remanent {
    rel_pos: Isometry2<f32>,
    resting_on_handle: object::ColliderHandle,
}

impl Remanent {
    pub fn new(rel_pos: Isometry2<f32>, resting_on_handle: object::ColliderHandle) -> Self {
        Self {
            rel_pos,
            resting_on_handle,
        }
    }

    pub fn rel_pos(&self) -> Isometry2<f32> {
        self.rel_pos
    }

    pub fn resting_on_handle(&self) -> object::ColliderHandle {
        self.resting_on_handle
    }
}
