use std::ops::Neg;

use ggez::{
    event,
    event::{Axis, Button, Keycode, Mod},
    graphics, timer, Context, GameResult,
};

use specs::prelude::*;

use crate::{
    assets::Assets,
    common as cmn,
    consts::*,
    game::{
        components,
        resources::{self, ProjectileKey, TileKey},
        systems,
    },
};

#[derive(Clone, Copy, PartialEq)]
enum State {
    Game,
    Pause,
}

impl Neg for State {
    type Output = State;

    fn neg(self) -> Self {
        match self {
            State::Game => State::Pause,
            State::Pause => State::Game,
        }
    }
}

pub struct GameState {
    world: World,
    dispatcher: Dispatcher<'static, 'static>,
    state: State,
}

impl GameState {
    pub fn new(ctx: &mut Context) -> GameResult<GameState> {
        let mut world = World::new();

        GameState::register_components(&mut world);

        let map = resources::Map::new(&mut world);

        world.add_resource(map);
        world.add_resource(resources::PhysicsWorld::new());
        world.add_resource(resources::AnimationTime::default());
        world.add_resource(resources::Tiles::new());
        world.add_resource(resources::Projectiles::new());

        let dispatcher: Dispatcher = DispatcherBuilder::new()
            .with(systems::TileDamageDealer, "tile_damage_dealer", &[])
            .with(systems::LadderClimbing, "ladder_climbing", &[])
            .with(systems::TrapOpener, "trap_opener", &[])
            .with(systems::TrapCloser, "trap_closer", &["trap_opener"])
            .with(systems::Jump, "jump", &["trap_opener"])
            .with(systems::JumpActionManager, "jump_action_manager", &["jump"])
            .with(systems::AimingDirection, "aiming_direction", &[])
            .with(systems::AimingTile, "aiming_tile", &[])
            .with(systems::PlayerMovement, "player_movement", &["jump"])
            .with(systems::PlayerShooting, "player_shooting", &[])
            .with(systems::Animations, "animations", &[])
            .with(systems::Construction, "construction", &[])
            .with(
                systems::ConstructionAppearing,
                "construction_appearing",
                &[],
            )
            .with(
                systems::ConstructionDisappearing,
                "construction_disappearing",
                &[],
            )
            .with(
                systems::DisplaySelectorManager,
                "display_selector_manager",
                &[],
            )
            .with(
                systems::SelectorChanger,
                "selector_changer",
                &["display_selector_manager"],
            )
            .with(systems::ProjectileRotation, "projectile_rotation", &[])
            .with(systems::ProjectileCollision, "projectile_collision", &[])
            .with(systems::ClockStepper, "clock_stepper", &[])
            .with(systems::ClockEntityRemover, "clock_entity_remover", &[])
            .with(systems::ClockDamagedRemover, "clock_damaged_remover", &[])
            .with(systems::PlayerLife, "player_life", &[])
            .build();

        systems::SceneCreator::new().run_now(&world.res);

        Ok(GameState {
            world,
            dispatcher,
            state: State::Game,
        })
    }

    fn register_components(world: &mut World) {
        world.register::<cmn::components::Image>();
        world.register::<cmn::components::Controllable>();
        world.register::<components::ColliderHandle>();
        world.register::<components::RigidBodyHandle>();
        world.register::<components::SensorHandle>();
        world.register::<components::Construction>();
        world.register::<components::Destruction>();
        world.register::<cmn::components::PlayerProfile>();
        world.register::<cmn::components::InputControl>();
        world.register::<components::Selector<TileKey>>();
        world.register::<components::Selector<ProjectileKey>>();
        world.register::<components::Tile>();
        world.register::<components::TileActionJump>();
        world.register::<components::TileActionTrapOpen>();
        world.register::<components::TileActionTrapClose>();
        world.register::<components::TileActionClimb>();
        world.register::<components::TileActionDealDamage>();
        world.register::<components::FacingDirection>();
        world.register::<components::FacingTile>();
        world.register::<cmn::components::Animable>();
        world.register::<components::Projectile>();
        world.register::<components::ProjectileRotation>();
        world.register::<components::ProjectileActionDamage>();
        world.register::<components::Remanent>();
        world.register::<components::Clock>();
        world.register::<components::TimerActionRemove>();
        world.register::<components::Damaged>();
        world.register::<components::Life>();
        world.register::<components::PeaceWall>();
    }

    pub fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        match self.state {
            State::Game => {
                self.world
                    .write_resource::<resources::PhysicsWorld>()
                    .world_mut()
                    .step();

                self.dispatcher.dispatch(&mut self.world.res);
                self.world.maintain();
            }
            State::Pause => {
                // TODO: Manage the menu
            }
        }

        Ok(())
    }

    pub fn draw(&mut self, ctx: &mut Context, assets: &Assets) -> GameResult<()> {
        systems::EntityRenderer::new(ctx, assets).run_now(&self.world.res);
        systems::FacingTileRenderer::new(ctx).run_now(&self.world.res);
        systems::SelectorRenderer::new(ctx, assets).run_now(&self.world.res);
        systems::AimingRenderer::new(ctx).run_now(&self.world.res);
        systems::LifeRenderer::new(ctx, assets).run_now(&self.world.res);
        systems::PeaceWallRenderer::new(ctx).run_now(&self.world.res);

        if self.state == State::Pause {
            // TODO: Draw the menu
        }

        Ok(())
    }

    pub fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: Keycode,
        _keymod: Mod,
        repeat: bool,
    ) {
        if keycode == Keycode::Backspace {
            self.state = -self.state;
        }

        cmn::systems::InputController::with_keycode(keycode, true).run_now(&self.world.res);
    }

    pub fn key_up_event(
        &mut self,
        _ctx: &mut Context,
        keycode: Keycode,
        _keymod: Mod,
        repeat: bool,
    ) {
        cmn::systems::InputController::with_keycode(keycode, false).run_now(&self.world.res);
    }

    pub fn controller_button_down_event(
        &mut self,
        _ctx: &mut Context,
        btn: Button,
        instance_id: i32,
    ) {
        cmn::systems::InputController::with_btn(instance_id, btn, true).run_now(&self.world.res);
    }

    pub fn controller_button_up_event(
        &mut self,
        _ctx: &mut Context,
        btn: Button,
        instance_id: i32,
    ) {
        cmn::systems::InputController::with_btn(instance_id, btn, false).run_now(&self.world.res);
    }

    pub fn controller_axis_event(
        &mut self,
        _ctx: &mut Context,
        axis: Axis,
        value_norm: f32,
        instance_id: i32,
    ) {
        cmn::systems::InputController::with_axis(instance_id, axis, value_norm)
            .run_now(&self.world.res);
    }
}
