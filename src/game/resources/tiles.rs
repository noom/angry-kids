use std::collections::HashMap;

use specs::prelude::*;

use nphysics2d::object::ColliderDesc;

use crate::{
    consts::*,
    assets::{AssetKey, Selectable},
    game::{
        components,
        entities::ColliderTag,
    },
};

pub struct TileInfo {
    asset_key: AssetKey,
    size: (f32, f32),
}

impl TileInfo {
    pub fn new(asset_key: AssetKey, size: (f32, f32)) -> Self {
        Self { asset_key, size }
    }

    pub fn asset_key(&self) -> &AssetKey {
        &self.asset_key
    }

    pub fn size(&self) -> &(f32, f32) {
        &self.size
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub enum TileKey {
    Void,
    ConstructionWeak,
    ConstructionStrong,
    Trampoline,
    TrapClosed,
    TrapOpened,
    Ladder,
    Spikes,
}

pub struct Tiles {
    tiles: HashMap<TileKey, TileInfo>,
}

impl Tiles {
    pub fn new() -> Self {
        let mut tiles: HashMap<TileKey, TileInfo> = HashMap::new();

        tiles.insert(
            TileKey::ConstructionWeak,
            TileInfo::new(AssetKey::Grass, (TILE_SIZE, TILE_SIZE)),
        );

        tiles.insert(
            TileKey::ConstructionStrong,
            TileInfo::new(AssetKey::Cobblestone, (TILE_SIZE, TILE_SIZE)),
        );

        tiles.insert(
            TileKey::Trampoline,
            TileInfo::new(AssetKey::Trampoline, (TILE_SIZE, TILE_SIZE / 2.)),
        );

        tiles.insert(
            TileKey::TrapClosed,
            TileInfo::new(AssetKey::TrapClosed, (TILE_SIZE, TILE_SIZE)),
        );

        tiles.insert(
            TileKey::TrapOpened,
            TileInfo::new(AssetKey::TrapOpened, (TILE_SIZE, TILE_SIZE)),
        );

        tiles.insert(
            TileKey::Ladder,
            TileInfo::new(AssetKey::Ladder, (TILE_SIZE, TILE_SIZE)),
        );

        tiles.insert(
            TileKey::Spikes,
            TileInfo::new(AssetKey::Spikes, (TILE_SIZE, TILE_SIZE)),
        );

        Self { tiles }
    }

    pub fn setup_collider(collider_desc: ColliderDesc<f32>, key: &TileKey) -> ColliderDesc<f32> {
        match key {
            TileKey::Trampoline => collider_desc.user_data(ColliderTag::ForceCollision),
            TileKey::TrapOpened | TileKey::Ladder | TileKey::Spikes => collider_desc
                .user_data(ColliderTag::IgnoreCollision)
                .sensor(true),
            _ => collider_desc,
        }
    }

    pub fn add_tags(builder: &Read<'_, LazyUpdate>, entity: Entity, key: &TileKey) {
        match key {
            TileKey::Trampoline => builder.insert(entity, components::TileActionJump),
            TileKey::TrapClosed => builder.insert(entity, components::TileActionTrapOpen),
            TileKey::TrapOpened => builder.insert(entity, components::TileActionTrapClose),
            TileKey::Ladder => builder.insert(entity, components::TileActionClimb),
            TileKey::Spikes => builder.insert(entity, components::TileActionDealDamage),
            _ => (),
        }
    }

    pub fn size_for(&self, key: &TileKey) -> &(f32, f32) {
        self.tile_info_for(key).size()
    }

    fn tile_info_for(&self, key: &TileKey) -> &TileInfo {
        self.tiles
            .get(key)
            .expect(&format!("Couldn't get {:?} from tiles resource", key))
    }
}

impl Selectable<TileKey> for Tiles {
    fn asset_key_for(&self, key: &TileKey) -> &AssetKey {
        self.tile_info_for(key).asset_key()
    }
}

impl Default for Tiles {
    fn default() -> Self {
        let tiles: HashMap<TileKey, TileInfo> = HashMap::new();

        Self { tiles }
    }
}
