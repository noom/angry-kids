pub struct AnimationTime(u32);

impl AnimationTime {
    pub fn step(&mut self) {
        self.0 += 1;
    }

    pub fn value(&self) -> u32 {
        self.0
    }
}

impl Default for AnimationTime {
    fn default() -> Self {
        Self(0)
    }
}
