use specs::prelude::*;

use crate::{game::{entities}, consts::*};

pub struct Map {
    map: Vec<Vec<Entity>>,
}

impl Map {
    pub fn new(world: &mut World) -> Self {
        let mut map: Vec<Vec<Entity>> = Vec::new();

        for _ in 0..MAP_WH.1 {
            let mut new_line: Vec<Entity> = Vec::new();

            for _ in 0..MAP_WH.0 {
                let entity = entities::create_tile(world.create_entity(), None);
                new_line.push(entity);
            }

            map.push(new_line);
        }

        Self { map }
    }

    pub fn entity_at_xy(&self, x: i32, y: i32) -> Option<Entity> {
        if x < 0 || x > MAP_WH.0 as i32 - 1 || y < 0 || y > MAP_WH.1 as i32 - 1 {
            None
        } else {
            Some(self.map[y as usize][x as usize])
        }
    }

    pub fn entity_at_px(&self, x: f32, y: f32) -> Option<Entity> {
        let x = (x.floor() / SCALE_FACTOR) as usize;
        let y = (y.floor() / SCALE_FACTOR) as usize;

        if x > MAP_WH.0 - 1 || y > MAP_WH.1 - 1 {
            None
        } else {
            Some(self.map[y][x])
        }
    }

    pub fn update_entity_at_xy(&mut self, new_entity: Entity, x: i32, y: i32) {
        if x < 0 || x > MAP_WH.0 as i32 - 1 || y < 0 || y > MAP_WH.1 as i32 - 1 {
            println!(
                "Cannot update entity at position {}, {}: position is outside of the map",
                x, y
            ); // TODO: Return an Error
        } else {
            self.map[y as usize][x as usize] = new_entity;
        }
    }

    pub fn update_entity_at_px(&mut self, new_entity: Entity, x: f32, y: f32) {
        let x = (x / SCALE_FACTOR) as usize;
        let y = (y / SCALE_FACTOR) as usize;

        if x > MAP_WH.0 - 1 || y > MAP_WH.1 - 1 {
            println!(
                "Cannot update entity at position {}, {}: position is outside of the map",
                x, y
            ); // TODO: Return an Error
        } else {
            self.map[y as usize][x as usize] = new_entity;
        }
    }
}

impl Default for Map {
    fn default() -> Self {
        let map: Vec<Vec<Entity>> = Vec::new();

        Self { map }
    }
}
