use nphysics2d::{
    object::{BodyPartHandle, ColliderDesc},
    world::World as NphysicsWorld,
};

use ncollide2d::shape::{Cuboid, ShapeHandle};

use nalgebra::{Isometry2, Vector2};

use crate::consts::*;

pub struct PhysicsWorld(NphysicsWorld<f32>);

impl PhysicsWorld {
    pub fn new() -> Self {
        let mut world = NphysicsWorld::new();
        world.set_gravity(Vector2::new(0.0, GRAVITY));

        for i in 0..4 {
            PhysicsWorld::add_border(&mut world, i);
        }

        Self(world)
    }

    pub fn world(&self) -> &NphysicsWorld<f32> {
        &self.0
    }

    pub fn world_mut(&mut self) -> &mut NphysicsWorld<f32> {
        &mut self.0
    }

    fn add_border(world: &mut NphysicsWorld<f32>, border_nb: u8) {
        let vertical = border_nb & 2 != 0;
        let end_of_screen = border_nb & 1 != 0;

        let width = if !vertical {
            NPHYSICS_WORLD_WH.0 as f32 / 2. + 1. - COLLIDER_MARGIN
        } else {
            1. - COLLIDER_MARGIN
        };

        let height = if vertical {
            NPHYSICS_WORLD_WH.1 as f32 / 2. + 1. - COLLIDER_MARGIN
        } else {
            1. - COLLIDER_MARGIN
        };

        let x = match (vertical, end_of_screen) {
            (false, _) => NPHYSICS_WORLD_WH.0 as f32 / 2.,
            (true, false) => -width,
            (true, true) => NPHYSICS_WORLD_WH.0 as f32 + width,
        };

        let y = match (vertical, end_of_screen) {
            (true, _) => -NPHYSICS_WORLD_WH.1 as f32 / 2.,
            (false, false) => height,
            (false, true) => -NPHYSICS_WORLD_WH.1 as f32 - height,
        };

        ColliderDesc::new(ShapeHandle::new(Cuboid::new(Vector2::new(width, height))))
            .position(Isometry2::new(Vector2::new(x, y), 0.0))
            .margin(COLLIDER_MARGIN)
            .build_with_parent(BodyPartHandle::ground(), world)
            .unwrap();
    }
}

impl Default for PhysicsWorld {
    fn default() -> Self{
        let world = NphysicsWorld::new();

        Self(world)
    }
}
