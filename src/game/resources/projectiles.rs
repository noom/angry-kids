use std::collections::HashMap;

use specs::prelude::*;

use nphysics2d::algebra::Velocity2;

use nalgebra::{Isometry2, Vector2};

use crate::{
    assets::{AssetKey, Selectable},
    game::components,
};

pub struct ProjectileInfo {
    asset_key: AssetKey,
    size: (f32, f32),
}

impl ProjectileInfo {
    pub fn new(asset_key: AssetKey, size: (f32, f32)) -> Self {
        Self { asset_key, size }
    }

    pub fn asset_key(&self) -> &AssetKey {
        &self.asset_key
    }

    pub fn size(&self) -> &(f32, f32) {
        &self.size
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum ProjectileKey {
    Syringe,
    BrokenBottle,
}

pub struct Projectiles {
    projectiles: HashMap<ProjectileKey, ProjectileInfo>,
}

impl Projectiles {
    pub fn new() -> Self {
        let mut projectiles: HashMap<ProjectileKey, ProjectileInfo> = HashMap::new();

        projectiles.insert(
            ProjectileKey::Syringe,
            ProjectileInfo::new(AssetKey::RedSyringe, (0.2, 0.6)),
        );

        projectiles.insert(
            ProjectileKey::BrokenBottle,
            ProjectileInfo::new(AssetKey::BrokenBottle, (0.2, 0.6)),
        );

        Self { projectiles }
    }

    pub fn initial_velocity_for(key: &ProjectileKey, aiming: Vector2<f32>) -> Velocity2<f32> {
        match key {
            ProjectileKey::BrokenBottle => {
                Velocity2::new(Vector2::new(aiming.x * 13.25, aiming.y * 12.), 10.)
            }
            ProjectileKey::Syringe => {
                Velocity2::new(Vector2::new(aiming.x * 20., aiming.y * 16.75), 0.)
            }
        }
    }

    pub fn add_tags(builder: &Read<'_, LazyUpdate>, entity: Entity, key: &ProjectileKey) {
        match key {
            ProjectileKey::Syringe => {
                builder.insert(entity, components::ProjectileRotation);
                builder.insert(entity, components::ProjectileActionDamage(1))
            }
            ProjectileKey::BrokenBottle => {
                builder.insert(entity, components::ProjectileActionDamage(2))
            }
        }
    }

    pub fn size_for(&self, key: &ProjectileKey) -> &(f32, f32) {
        self.projectile_info_for(key).size()
    }

    fn projectile_info_for(&self, key: &ProjectileKey) -> &ProjectileInfo {
        self.projectiles
            .get(key)
            .expect(&format!("Couldn't get {:?} from projectiles resource", key,))
    }
}

impl Selectable<ProjectileKey> for Projectiles {
    fn asset_key_for(&self, key: &ProjectileKey) -> &AssetKey {
        self.projectile_info_for(key).asset_key()
    }
}

impl Default for Projectiles {
    fn default() -> Self {
        let projectiles: HashMap<ProjectileKey, ProjectileInfo> = HashMap::new();

        Self { projectiles }
    }
}
