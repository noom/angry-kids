use ggez::{
    event,
    event::{Axis, Button, Keycode, Mod},
    graphics, timer, Context, GameResult,
};

use crate::{assets::Assets, consts::*, game::GameState, menu::MenuState};

#[derive(Clone, Copy, PartialEq)]
enum State {
    Menu,
    Game,
}

pub struct GameManager {
    state: State,
    assets: Assets,
    menu_state: MenuState,
    game_state: GameState,
    tick: i32,
}

impl GameManager {
    pub fn new(ctx: &mut Context) -> GameResult<GameManager> {
        let state = State::Game;
        let assets = Assets::new(ctx);
        let menu_state = MenuState::new(ctx)?;
        let game_state = GameState::new(ctx)?;

        Ok(GameManager {
            state,
            assets,
            menu_state,
            game_state,
            tick: 0,
        })
    }
}

impl event::EventHandler for GameManager {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        while timer::check_update_time(ctx, FPS) {
            match self.state {
                State::Menu => self.menu_state.update(ctx)?,
                State::Game => self.game_state.update(ctx)?,
            }

            self.tick += 1;
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        if self.tick > 60 {
            println!("FPS: {:.*}", 2, timer::get_fps(ctx));

            self.tick = 0;
        }

        graphics::clear(ctx);

        match self.state {
            State::Menu => self.menu_state.draw(ctx, &self.assets)?,
            State::Game => self.game_state.draw(ctx, &self.assets)?,
        }

        graphics::present(ctx);

        Ok(())
    }

    fn key_down_event(&mut self, ctx: &mut Context, keycode: Keycode, keymod: Mod, repeat: bool) {
        if repeat {
            return;
        }

        if keycode == Keycode::Escape {
            ctx.quit().unwrap();
        }

        match self.state {
            State::Menu => self.menu_state.key_down_event(ctx, keycode, keymod, repeat),
            State::Game => self.game_state.key_down_event(ctx, keycode, keymod, repeat),
        }
    }

    fn key_up_event(&mut self, ctx: &mut Context, keycode: Keycode, keymod: Mod, repeat: bool) {
        if repeat {
            return;
        }

        match self.state {
            State::Menu => self.menu_state.key_up_event(ctx, keycode, keymod, repeat),
            State::Game => self.game_state.key_up_event(ctx, keycode, keymod, repeat),
        }
    }

    fn controller_button_down_event(&mut self, ctx: &mut Context, btn: Button, instance_id: i32) {
        match self.state {
            State::Menu => self
                .menu_state
                .controller_button_down_event(ctx, btn, instance_id),
            State::Game => self
                .game_state
                .controller_button_down_event(ctx, btn, instance_id),
        }
    }

    fn controller_button_up_event(&mut self, ctx: &mut Context, btn: Button, instance_id: i32) {
        match self.state {
            State::Menu => self
                .menu_state
                .controller_button_up_event(ctx, btn, instance_id),
            State::Game => self
                .game_state
                .controller_button_up_event(ctx, btn, instance_id),
        }
    }

    fn controller_axis_event(
        &mut self,
        ctx: &mut Context,
        axis: Axis,
        value: i16,
        instance_id: i32,
    ) {
        let value_norm = value as f32 / 32767.;

        match self.state {
            State::Menu => self
                .menu_state
                .controller_axis_event(ctx, axis, value_norm, instance_id),
            State::Game => self
                .game_state
                .controller_axis_event(ctx, axis, value_norm, instance_id),
        }
    }
}
